﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPM.MetaData
{
    public class DataFactory
    {
        private static DataFactory instans = new DataFactory();
        public static DataFactory GetInstans() { return instans; }

        public string METADATA_GET_ONLINE(string METHODBASE = "", string MASSAGE = "")
        {
            var data = new
            {
                METHODBASE = METHODBASE,
                MASSAGE = MASSAGE,
            };

            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public string METADATA_GET_ERROR(string METHODBASE = "", string MASSAGE = "", string STACKTRACE = "")
        {
            var data = new
            {
                METHODBASE = METHODBASE,
                MASSAGE = MASSAGE,
                STACKTRACE = STACKTRACE
            };

            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public string METADATA_GET_NorwegianCustomer(
           string SignUpDate = "",
           string OtherRef = "",
           string NorwegianRef = "",
           string CustomerFullName = "",

           string CustomerFullAlternativeName = "",

           string CustomerCPR = "",
           string CustomerAdress = "",
           string CustomerPhone = "",

           string CustomerEmail = "",
           string HasBeenProcessed = "",
           string HasBeenProcessedDate = "",
           string LOG_STATUS = "")
        {
            var data = new
            {
                SignUpDate = SignUpDate,
                OtherRef = OtherRef,
                NorwegianRef = NorwegianRef,
                CustomerFullName = CustomerFullName,

                CustomerFullAlternativeName = CustomerFullAlternativeName,

                CustomerCPR = CustomerCPR,
                CustomerAdress = CustomerAdress,
                CustomerPhone = CustomerPhone,

                CustomerEmail = CustomerEmail,
                HasBeenProcessed = HasBeenProcessed,
                HasBeenProcessedDate = HasBeenProcessedDate,
                LOG_STATUS = LOG_STATUS
            };

            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public string METADATA_GET_CUSTOMER(
            string CUSTOMER_ID = "",
            string CUSTOMER_LBNR = "",
            string CUSTOMER_FIRST_NAME = "",
            string CUSTOMER_LAST_NAME = "",

            string CUSTOMER_CVR = "",

            string CUSTOMER_EMAIL = "",
            string CUSTOMER_PHONE = "",
            string CUSTOMER_MOBILE = "",

            string CUSTOMER_POSTAL_CODE = "",
            string CUSTOMER_CITY_NAME = "",
            string CUSTOMER_ADDRESS_1 = "",
            string CUSTOMER_ADDRESS_2 = "",
            string CUSTOMER_HOUSE_NUMBER = "",

            string CUSTOMER_INKASSO = "",
            string CUSTOMER_ALLOW_DGK = "",
            string CUSTOMER_CONTRACTS = "",
            string CUSTOMER_SALDO = "",

            string CUSTOMER_IS_BLACK_LISTED = "",
            string CUSTOMER_IS_ALREADY_EXISTING = "",
            string CUSTOMER_PRODUCT_NAME = "",

            string CONTACT_TIME = "",
            string KWH_USAGE = "",
            string CUSTOMER_CONTRACT_START_DATE = "",
            string CUSTOMER_CONTRACT_END_DATE = "",
            string CONTRACT_BINDING = "",
            List<string> CUSTOMER_ALL_CONTRACTS = null,
            List<SomeDTO> SomeDTOs = null,

            string LOG_STATUS = "")
        {
            var data = new
            {
                CUSTOMER_ID = CUSTOMER_ID,
                CUSTOMER_LBNR = CUSTOMER_LBNR,
                CUSTOMER_FIRST_NAME = CUSTOMER_FIRST_NAME,
                CUSTOMER_LAST_NAME = CUSTOMER_LAST_NAME,

                CUSTOMER_CVR = CUSTOMER_CVR,

                CUSTOMER_EMAIL = CUSTOMER_EMAIL,
                CUSTOMER_PHONE = CUSTOMER_PHONE,
                CUSTOMER_MOBILE = CUSTOMER_MOBILE,

                CUSTOMER_POSTAL_CODE = CUSTOMER_POSTAL_CODE,
                CUSTOMER_CITY_NAME = CUSTOMER_CITY_NAME,
                CUSTOMER_ADDRESS_1 = CUSTOMER_ADDRESS_1,
                CUSTOMER_ADDRESS_2 = CUSTOMER_ADDRESS_2,
                CUSTOMER_HOUSE_NUMBER = CUSTOMER_HOUSE_NUMBER,

                CUSTOMER_INKASSO = CUSTOMER_INKASSO,
                CUSTOMER_ALLOW_DGK = CUSTOMER_ALLOW_DGK,
                CUSTOMER_CONTRACTS = CUSTOMER_CONTRACTS,
                CUSTOMER_SALDO = CUSTOMER_SALDO,

                CUSTOMER_IS_BLACK_LISTED = CUSTOMER_IS_BLACK_LISTED,
                CUSTOMER_IS_ALREADY_EXISTING = CUSTOMER_IS_ALREADY_EXISTING,
                CUSTOMER_PRODUCT_NAME = CUSTOMER_PRODUCT_NAME,

                CONTACT_TIME = CONTACT_TIME,
                KWH_USAGE = KWH_USAGE,
                CUSTOMER_CONTRACT_START_DATE = CUSTOMER_CONTRACT_START_DATE,
                CUSTOMER_CONTRACT_END_DATE = CUSTOMER_CONTRACT_END_DATE,
                CONTRACT_BINDING = CONTRACT_BINDING,

                CUSTOMER_ALL_CONTRACTS = CUSTOMER_ALL_CONTRACTS,
                SomeDTOs = SomeDTOs,

                LOG_STATUS = LOG_STATUS,
            };


            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }


    }
}