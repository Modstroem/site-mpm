﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPM.Authentication
{
    public class Authentication
    {
        public int ClientID { get; set; }
        public string Name { get; set; }
        public string AIPKey { get; set; }
        public string EncryptionKey { get; set; }
        public long MaxConnections { get; set; }
        public AuthenticationStatus Status { get; set; }

        public Authentication()
        {

        }

        public Authentication(string Name, string AIPKey, string EncryptionKey, int MaxConnections, AuthenticationStatus Status)
        {
            this.Name = Name;
            this.AIPKey = AIPKey;
            this.EncryptionKey = EncryptionKey;
            this.MaxConnections = MaxConnections;
            this.Status = Status;
        }
    }
}