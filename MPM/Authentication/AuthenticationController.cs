﻿using EncryptionLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MPM.Authentication
{
    public class AuthenticationController
    {
        private List<Authentication> externalAuthorisationList;
        private List<Authentication> internalAuthorisationList;
       

        public string Message = "";

        public AuthenticationController()
        {
            externalAuthorisationList = new List<Authentication>();
            internalAuthorisationList = new List<Authentication>();
            LoadAuthorisations();
        }

        public AuthenticationController(string empty)
        {

        }

        public List<Authentication> LoadClients(int ClientType)
        {
            List<Authentication> result = new List<Authentication>();

            if (ClientType == 100)
            {
                try
                {
                    string userName = ConfigurationManager.AppSettings["ExternalClientUser"];
                    string passWord = ConfigurationManager.AppSettings["ExternalClientPssword"];
                    string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                    string serverName = ConfigurationManager.AppSettings["ServerName"];

                    //WTF ! No time
                    if (userName == null || passWord == null || databaseName == null || serverName == null)
                    {
                        userName = "ExternalClient";
                        passWord = "C97xwGqO1";
                        databaseName = "MIIS";
                        serverName = "SPSQL004";
                    }

                    if (userName != null && passWord != null && databaseName != null && serverName != null)
                    {
                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = "Data Source=" + serverName + ";" + "Initial Catalog=" + databaseName + ";" + "User id=" + userName + ";" + "Password=" + passWord + ";";
                        conn.Open();
                        result = new List<Authentication>();
                        SqlCommand command = new SqlCommand("SELECT * from [MIIS].[dbo].[API_Client] where ClientType=" + ClientType, conn);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Authentication a = new Authentication();
                                a.ClientID = Convert.ToInt32(reader["ClientId"].ToString());
                                a.Name = reader["Name"].ToString();
                                a.AIPKey = reader["AIPKey"].ToString();
                                a.EncryptionKey = reader["EncryptionKey"].ToString();
                                a.MaxConnections = Convert.ToInt64(reader["MaxConnections"].ToString());
                                string s = reader["Status"].ToString();
                                a.Status = (AuthenticationStatus)Enum.Parse(typeof(AuthenticationStatus), s);
                                result.Add(a);
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Message += ex.Message + " : " + ex.StackTrace + "LoadClients ClientType " + ClientType;
                }
            }
            else if (ClientType == 200)
            {
                try
                {
                    string userName = ConfigurationManager.AppSettings["InternalClientUser"];
                    string passWord = ConfigurationManager.AppSettings["InternalClientPssword"];
                    string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                    string serverName = ConfigurationManager.AppSettings["ServerName"];

                    if (userName == null || passWord == null || databaseName == null || serverName == null)
                    {
                        userName = "InternalClient";
                        passWord = "C97xwGqO2";
                        databaseName = "MIIS";
                        serverName = "SPSQL004";
                    }


                    if (userName != null && passWord != null && databaseName != null && serverName != null)
                    {
                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = "Data Source=" + serverName + ";" + "Initial Catalog=" + databaseName + ";" + "User id=" + userName + ";" + "Password=" + passWord + ";";
                        conn.Open();
                        result = new List<Authentication>();
                        SqlCommand command = new SqlCommand("SELECT * from [MIIS].[dbo].[API_Client] where ClientType=" + ClientType, conn);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Authentication a = new Authentication();
                                a.ClientID = Convert.ToInt32(reader["ClientId"].ToString());
                                a.Name = reader["Name"].ToString();
                                a.AIPKey = reader["AIPKey"].ToString();
                                a.EncryptionKey = reader["EncryptionKey"].ToString();
                                a.MaxConnections = Convert.ToInt64(reader["MaxConnections"].ToString());
                                string s = reader["Status"].ToString();
                                a.Status = (AuthenticationStatus)Enum.Parse(typeof(AuthenticationStatus), s);
                                result.Add(a);
                            }
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    Message += ex.Message + " : " + ex.StackTrace + "LoadClients ClientType " + ClientType;
                }
            }
            else
            {

            }

            return result;
        }

        private void LoadAuthorisations()
        {
            try
            {
               
                internalAuthorisationList = LoadClients(100);
                if (Message != "")
                {
                    Message += Message + "LoadAuthorisations ";
                }
                externalAuthorisationList = LoadClients(200);
                if (Message != "")
                {
                    Message += Message + "LoadAuthorisations ";
                }
            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "LoadAuthorisations";
            }
        }

        public bool ChangeEncryptionKey(string AIPKey, string NewEncryption_Key, int ClientType)
        {
            bool result = false;

            try
            {
                if (ClientType == 100)
                {
                    try
                    {

                        string userName = ConfigurationManager.AppSettings["InternalClientUser"];
                        string passWord = ConfigurationManager.AppSettings["InternalClientPssword"];
                        string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                        string serverName = ConfigurationManager.AppSettings["ServerName"];
                        
                        if (userName != null && passWord != null && databaseName != null && serverName != null)
                        {
                            SqlConnection conn = new SqlConnection();
                            // var connection = System.Configuration.ConfigurationManager.ConnectionStrings["currentMSDB"].ConnectionString;

                            conn.ConnectionString = "Data Source=" + serverName + ";" + "Initial Catalog=" + databaseName + ";" + "User id=" + userName + ";" + "Password=" + passWord + ";";
                            conn.Open();

                            SqlCommand command = new SqlCommand("UPDATE [MIIS].[dbo].[API_Client]  SET EncryptionKey = '" + NewEncryption_Key + "' where AIPKey like'" + AIPKey + "'", conn);

                            int r = command.ExecuteNonQuery();
                            if (r == 1)
                            {
                                result = true;
                            }

                            conn.Close();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else if (ClientType == 200)
                {
                    try
                    {
                        string userName = ConfigurationManager.AppSettings["ExternalClientUser"];
                        string passWord = ConfigurationManager.AppSettings["ExternalClientPssword"];
                        string databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                        string serverName = ConfigurationManager.AppSettings["ServerName"];

                        if (userName != null && passWord != null && databaseName != null && serverName != null)
                        {
                            SqlConnection conn = new SqlConnection();
                            conn.ConnectionString = "Data Source=" + serverName + ";" + "Initial Catalog=" + databaseName + ";" + "User id=" + userName + ";" + "Password=" + passWord + ";";
                            conn.Open();

                            SqlCommand command = new SqlCommand("UPDATE [MIIS].[dbo].[API_Client]  SET EncryptionKey = '" + NewEncryption_Key + "' where AIPKey like'" + AIPKey + "'", conn);

                            int r = command.ExecuteNonQuery();
                            if (r == 1)
                            {
                                result = true;
                            }

                            conn.Close();
                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {

                }

            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public bool ChangeInternalEncryptionKey(string AIPKey, string NewEncryption_Key)
        {
            bool result = false;

            try
            {

                if (internalAuthorisationList.Count == 0)
                {
                    result = false;
                }
                else
                {
                    Authentication authorisation = internalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {

                        Encryptor e = new Encryptor();
                        authorisation.EncryptionKey = e.Encrypt(NewEncryption_Key, NewEncryption_Key);
                       
                        result = ChangeEncryptionKey(AIPKey, authorisation.EncryptionKey, 100);


                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "ChangeInternalEncryptionKey";
            }

            return result;
        }


        public bool ChangeExternalEncryptionKey(string AIPKey, string NewEncryption_Key)
        {
            bool result = false;

            try
            {

                if (externalAuthorisationList.Count == 0)
                {
                    result = false;
                }
                else
                {
                    Authentication authorisation = externalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        Encryptor e = new Encryptor();
                        authorisation.EncryptionKey = e.Encrypt(NewEncryption_Key, NewEncryption_Key);
                      
                        result = ChangeEncryptionKey(AIPKey, authorisation.EncryptionKey, 200);
                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "ChangeExternalEncryptionKey";
            }

            return result;
        }

        public int AccessType(string AIPKey)
        {
            int result = 0;

            try
            {

                if (externalAuthorisationList.Count == 0 || internalAuthorisationList.Count == 0)
                {
                    result = -1;
                }
                else
                {
                    Authentication authorisation = externalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        result = 200;
                    }

                    authorisation = internalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (result != 200)
                    {
                        if (authorisation != null)
                        {
                            result = 100;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                result = -1;
                Message += ex.Message + " : " + ex.StackTrace + "ExternalAccessGranted";
            }

            return result;
        }

        public bool ExternalAccessGranted(string AIPKey, string Encryption_Key)
        {
            bool result = false;

            try
            {

                if (externalAuthorisationList.Count == 0)
                {
                    result = false;
                }
                else
                {
                    Authentication authorisation = externalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        if (authorisation.Status == AuthenticationStatus.Enabled)
                        {
                            Encryptor e = new Encryptor();
                            string Encrypted1 = e.Encrypt(Encryption_Key, Encryption_Key);
                            string Encrypted2 = authorisation.EncryptionKey;

                            string wer = e.Encrypt("Norwegian2019!Run", "Norwegian2019!Run");
                            if (Encrypted1 == Encrypted2)
                            {
                                if (e.Message != "")
                                {
                                    Message += e.Message + " : EncryptContent";
                                }
                                if (authorisation.EncryptionKey == e.Encrypt(Encryption_Key, Encryption_Key))
                                {
                                    if (authorisation.AIPKey == AIPKey)
                                    {
                                        result = true;
                                    }
                                }

                                if (e.Message != "")
                                {
                                    Message += e.Message + " : EncryptContent";
                                }
                                else
                                {
                                    //EncryptContent OK";
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "ExternalAccessGranted";
            }

            return result;
        }

        public bool InternalAccessGranted(string AIPKey, string Encryption_Key)
        {
            bool result = false;

            try
            {
                if (internalAuthorisationList.Count == 0)
                {
                    result = false;
                }
                else
                {
                    Authentication authorisation = internalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        if (authorisation.Status == AuthenticationStatus.Enabled)
                        {
                            Encryptor e = new Encryptor();
                            if (e.Message != "")
                            {
                                Message += e.Message + " : EncryptContent";
                            }

                            string v12 = e.Encrypt("AM", "AM");

                            string v1 = e.Encrypt(Encryption_Key, Encryption_Key);
                            string v2 = authorisation.EncryptionKey;

                            if (authorisation.EncryptionKey == e.Encrypt(Encryption_Key, Encryption_Key))
                            {
                                if (authorisation.AIPKey == AIPKey)
                                {
                                    result = true;
                                }
                            }

                            if (e.Message != "")
                            {
                                Message += e.Message + " : EncryptContent";
                            }
                            else
                            {
                                Message += e.Message + " : EncryptContent";
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "InternalAccessGranted";
            }

            return result;
        }
        public string EncryptContent(string AIPKey, string Encryption_Key, string Text)
        {
            string result = "";

            try
            {
                if (internalAuthorisationList.Count == 0)
                {
                    result = "";
                }
                else
                {
                    Authentication authorisation = internalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        if (authorisation.Status == AuthenticationStatus.Enabled)
                        {
                            Encryptor e = new Encryptor();
                            if (e.Message != "")
                            {
                                Message += e.Message + " : EncryptContent";
                            }
                            result = e.EncryptWidthPassword(Text, Encryption_Key);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "EncryptContent";
            }

            return result;
        }


        public string ScrambleContent(string Text)
        {
            string result = "";

            try
            {

                Encryptor e = new Encryptor();
                if (e.Message != "")
                {
                    Message += e.Message + " : EncryptContent";
                }

                result = e.EncryptWidthPassword(Text, "EncryptContent");

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "EncryptContent";
            }

            return result;
        }

        public string DecryptWidthPassword(string AIPKey, string Encryption_Key, string Text)
        {
            string result = "";

            try
            {
                if (internalAuthorisationList.Count == 0)
                {
                    result = "";
                }
                else
                {
                    Authentication authorisation = internalAuthorisationList.Find(x => x.AIPKey == AIPKey);
                    if (authorisation != null)
                    {
                        if (authorisation.Status == AuthenticationStatus.Enabled)
                        {
                            Encryptor e = new Encryptor();
                            if (e.Message != "")
                            {
                                Message += e.Message + " : EncryptContent";
                            }
                            result = e.DecryptWidthPassword(Text, Encryption_Key);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Message += ex.Message + " : " + ex.StackTrace + "DecryptWidthPassword";
            }

            return result;
        }
    }
}