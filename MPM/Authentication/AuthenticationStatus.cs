﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPM.Authentication
{
    public enum AuthenticationStatus
    {
        Disabled = 0,
        Enabled = 1
    }
}