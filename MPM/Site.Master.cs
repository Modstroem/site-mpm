﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    lbllogInd.Text = System.Web.HttpContext.Current.User.Identity.Name.Replace(@"AKMOD\", "").Replace(".", " "); ;
                }

                if (lbllogInd.Text != "")
                {
                    try
                    {

                        string[] names = lbllogInd.Text.Split(' ');
                        string name = "";

                        foreach (string n in names)
                        {
                            if (name == "")
                            {
                                name += FirstLetterToUpper(n) + " ";
                                continue;
                            }

                            name += FirstLetterToUpper(n);
                        }

                        lbllogInd.Text = name;

                        if (Session["MUSER"] == null)
                        {
                            MUSER I = new MUSER();
                            MUSER MUSER = I.GetUser(name);
                            if (MUSER != null)
                            {
                                Session["MUSER"] = MUSER;
                            }
                            else
                            {
                                Session["MUSER"] = I.GetStandartSettings(name);
                            }
                        }
                    }
                    catch { }
                }


            }

        }
        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return "";

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }
    }
}