﻿using MPM.Handlers;
using MPM.MetaData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;

namespace MPM.Controllers
{
    [RoutePrefix("NavController")]
    public class NavController : ApiController
    {
        private string ControllerName = "NavController";


        [Route("Introduction")] // OK
        [HttpGet]
        public HttpResponseMessage Introduction(string ApiKey, string EncryptionKey)
        {

            string MethodName = "Introduction";
            HttpResponseMessage responseMessage;
            try
            {

                Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
                if (responce.ResponceType != Enums.ResponceType.Ok)
                {
                    return responseMessage = ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType);
                }
                //LogIdentity(MethodName, ApiKey);
                Type myType = (typeof(NavController));
                MethodInfo[] Methods = myType.GetMethods();

                string CurrentUrl = "api";

                var content = @"<html><body><table>
                                  <tr>
                                    <th><img src='https://api.modstroem.dk/favicon.png' width='32' height='32' align='middle'</th>
                                    <th><font size='5'>[ Modstrøms external application programming interface ] API</font></th> 
                                  </tr>
                                </table>";


                content += @"</br><div style='position:fixed;top:0;right:0; width:300px;height:100px;margin:auto;'>
        <p><font color='#0000FF'>The projects inspiration moto:<font color='#0000FF'></font></p>
        <p><font color='#ff00ff'>Any intelligent fool can make things bigger and more complex... It takes a touch of genius - and a lot of courage - to move in the</br> opposite - direction.</font></p>
        <br/>
    </div>";//</br><div style='position:relative;top:0;bottom:0;left:0;right:0; width:300px;height:100px;margin:auto;'>


                string ApiKey_ = "ABB70AB0-D01B-4200-B4A2-1754413EE5E4";
                string EncryptionKey_ = "Norwegian2019!Run";

                try
                {



                    System.Web.HttpContext context = System.Web.HttpContext.Current;
                    ApiKey_ = GetApiKey(context.Request.Url.ToString());
                    EncryptionKey_ = GetEncryptionKey(context.Request.Url.ToString());
                    IPAddress myIP = IPAddress.Parse(context.Request.UserHostAddress);
                    IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                    List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();
                    string all = "<font color='#FF5733'>[" + context.Request.UserHostAddress + "]</font>";
                    CurrentUrl = GetCurrentUrl(context.Request.Url.ToString());

                    string v = context.User.Identity.Name;

                    if (v == null || v == "")
                    {
                        v = "";
                    }

                    foreach (string s in compName)
                    {
                        all += "[" + s + "]";
                    }
                    content += @"<p></p><table>
                                  <tr>
                                    <th><img src='https://api.modstroem.dk/hwoareyou.png' width='18' height='18' align='middle'</th>
                                    <th><font size='2'><font color='#FFBD33'>Service contacted by " + all + " Local : " + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz") + @"</font></th>" + v + @"
                                  </tr>
                                </table>";

                }
                catch
                {

                }

                content += @"<p>Controller name : <font color='#ff00ff'>" + ControllerName + "</font></b></p><p><b>Methods overview</b></p><p><font color='#000000'><b>For further explanation, contact Modstrøm headquarters.</b></font></p>";
                var contentAddOn = @"<script type='text/javascript'>  
                                    function togglediv(id) 
                                    {
                                        var div = document.getElementById(id);
                                        if(div == null)
                                        {
        
                                        }
                                        else
                                        {
                                            div.style.display = div.style.display == 'none' ? 'block' : 'none';
                                        }
                                    }
                                    </script>";


                contentAddOn += @"<style type='text/css'>
                                .btn {
                                    background-color:blue;
                                    cursor:pointer;
                                    height:20px;
                                    width:20px;
                                    background-color: white;
                                    margin: 4px 2px;
                                    text-align: center;
                                    text-color: blue;
                                    padding: 0;
                                    border: none;
                                    background: none;
                                }
                                </style>";


                string value = "";

                //bool first = true;
                int id = 1;
                for (int i = 0; i < Methods.Length; i++)
                {
                    MethodInfo myMethodInfo = (MethodInfo)Methods[i];

                    if (myMethodInfo.ToString().Contains("System.Net.Http.HttpResponseMessage"))
                    {
                        if (myMethodInfo.Name.ToString().Contains("ExecuteAsync")) { continue; }

                        value = "";

                        if (myMethodInfo.Name.ToLower() == "introduction")
                        {
                            value += @"<a></br>¤</a>
</br>
<table >";
                        }
                        else
                        {
                            value += @"<a></br>Documentation will follow as soon as possible.</a>
</br>
<table >";
                        }


                        string prop = "<font color='#C68E17'>( </font>";
                        int count = 0;
                        string target = "";
                        int textId = 1;
                        foreach (ParameterInfo pParameter in myMethodInfo.GetParameters())
                        {
                            count++;

                            if (pParameter.ParameterType.Name.Contains("DTO"))
                            {
                                prop += "<font color='#FF0000'>" + (pParameter.ParameterType.Name) + "</font> " + (pParameter.Name) + "<font color='#000000'> , </font>";
                            }
                            else
                            {
                                if (count == myMethodInfo.GetParameters().Length - 1)
                                {
                                    prop += "<font color='#0000FF'>" + (pParameter.ParameterType.Name.ToLower()) + "</font> " + (pParameter.Name) + "<font color='#000000'> , </font>";
                                }

                                else
                                {
                                    prop += "<font color='#0000FF'>" + (pParameter.ParameterType.Name.ToLower()) + "</font> " + (pParameter.Name) + "<font color='#000000'> , </font>";
                                }
                            }

                            //if (first == true)
                            //{
                            //    first = false;
                            //    continue;
                            //}

                            string Id = "textId" + textId;
                            if (pParameter.Name.ToLower() == "apikey")
                            {
                                target += pParameter.Name + "=" + ApiKey_ + "&";
                                value += @"<tr><td>" + (pParameter.Name) + ":</td><td ><input id='" + Id + "' type='text' name='" + (pParameter.Name) + "' value='" + ApiKey_ + @"'></td></tr>";

                            }
                            else if (pParameter.Name.ToLower() == "encryptionkey")
                            {
                                target += pParameter.Name + "=" + EncryptionKey_ + "&";
                                value += @"<tr><td>" + (pParameter.Name) + ":</td><td ><input id='" + Id + "' type='text' name='" + (pParameter.Name) + "' value='" + EncryptionKey_ + @"'></td> </tr>";

                            }
                            else if (pParameter.Name.ToLower() == "reset")
                            {
                                target += pParameter.Name + "=flase" + "&";
                                value += @"<tr><td>" + (pParameter.Name) + ":</td><td ><input id='" + Id + "' type='text' name='" + (pParameter.Name) + "' value='" + "=false" + @"'></td> </tr>";
                            }
                            else
                            {
                                if (pParameter.Name.ToLower().Contains("password"))
                                {
                                    value += @"<tr><td>" + (pParameter.Name) + ":</td><td ><input id='" + Id + "' type='password' name='" + (pParameter.Name) + "' value='" + "" + @"'></td> </tr>";
                                    target += pParameter.Name + "=" + "&";//document.getElementById('"+ Id + "').value
                                }
                                else
                                {

                                    value += @"<tr><td>" + pParameter.Name + ":</td><td ><input id='" + Id + "' type='text' name='" + pParameter.Name + "' value='" + "" + @"'></td> </tr>";
                                    target += pParameter.Name + "=REPLACEVALUE&";//document.getElementById('"+ Id + "').value
                                }
                            }

                            value += "</br>";
                            textId++;

                        }

                        if (target != "")
                        {
                            target = target.Remove(target.Length - 1, 1);
                        }

                        if (myMethodInfo.Name.ToLower() != "introduction")
                        {
                            value += "</table>";
                            value += "</br><a target='_blank' href='https://" + CurrentUrl + ".modstroem.dk/" + ControllerName + "/" + myMethodInfo.Name + "?" + target + "' class='button'>Generate Link</a></br></br>";//Exequte

                        }
                        else
                        {
                            value += "</table>";
                            value += "</br><a target='_blank' href='https://" + CurrentUrl + ".modstroem.dk/" + ControllerName + "/" + myMethodInfo.Name + "?" + target + "' class='button'>Generate Link</a></br></br>";

                            string doc = @"<a></br>All methods descriptions is automatically generated by the controller</a></br>
                            <a>The purpose of this is to make it simpler to use this on the fly.</a></br>
                            <a>Like in 'James Bond' movie (Skyfall 2010), all James Bond got from Q is gun and gps receiver.</a>
                            </br>
                            </br>";

                            value = value.Replace("¤", doc);
                        }


                        id++;
                        prop = prop.Remove(prop.Length - 9, 1);
                        prop += "<font color='#C68E17'>) </font>";
                        string uniquename = "uniquename" + id;
                        // prop += "<a href='#' onclick='togglediv(" + uniquename + ".id);'>Expand/Collapse</a>";
                        // prop += "<div id='" + uniquename + "' style='display:none;'>" + value + "</div>";

                        content += "<p><font color='#0000A0'>" + ("[HttpGet]") + "</font><font color='#E56717'>" + (myMethodInfo.Name) + "</font><font color='#008000'>" + prop + "</font></p>";
                        //content += prop;

                    }
                }


                content += contentAddOn;


                content += "</br></br></br></br></br></br></br></br></body></html>";

                responseMessage = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(content, System.Text.Encoding.UTF8, "text/html"),
                };
            }
            catch (Exception ex)
            {
                responseMessage = ExceptionHandler.ExceptionError(MethodName, ControllerName, ex);
            }

            return responseMessage;

        }

        private string GetCurrentUrl(string url)
        {
            string id = "";
            url = url.Replace("%3f", "?").Replace("%3d", "=");
            try
            {
                string[] ids = url.ToString().Split(new string[] { ".", "//" }, StringSplitOptions.None);
                id = ids[1];

            }
            catch { }

            return id;
        }

        private string GetApiKey(string url)
        {
            string id = "";
            url = url.Replace("%3f", "?").Replace("%3d", "=");
            try
            {

                string gid = "ApiKey=";
                if (url.ToString().Contains(gid))
                {
                    string[] ids = url.ToString().Split(new string[] { "ApiKey=", "&" }, StringSplitOptions.None);
                    id = ids[1];
                }
            }
            catch { }

            return id;
        }

        private string GetEncryptionKey(string url)
        {
            string id = "";
            url = url.Replace("%3f", "?").Replace("%3d", "=");
            try
            {

                string gid = "EncryptionKey=";
                if (url.ToString().Contains(gid))
                {
                    string[] ids = url.ToString().Split(new string[] { "EncryptionKey=", "&" }, StringSplitOptions.None);
                    id = ids[2];
                }
            }
            catch { }

            return id;
        }

        private void LogIdentity(string MethodName, string ApiKey)
        {
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                IPAddress myIP = IPAddress.Parse(context.Request.UserHostAddress);
                IPHostEntry GetIPHost = Dns.GetHostEntry(myIP);
                List<string> compName = GetIPHost.HostName.ToString().Split('.').ToList();

                string Identity = "";
                foreach (string s in compName)
                {
                    Identity += s + "|";
                }

                Identity = Identity.Remove(Identity.Length - 1, 1);

                UtilitiesLibrary.GetInstans().LogIdentity(context.Request.UserHostAddress.ToString(), Identity, MethodName, ControllerName, ApiKey);
            }
            catch
            {

            }
        }


        [Route("Is_Online")]
        [HttpGet]
        public HttpResponseMessage Is_Online(string ApiKey, string EncryptionKey)
        {
            HttpResponseMessage responseMessage;

            try
            {
                Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
                if (responce.ResponceType != Enums.ResponceType.Ok) { return responseMessage = ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType); }


                responseMessage = ResponceHandler.Responce("Ok", "SYSTEM  IS ONLINE", 1, 6);

            }
            catch (Exception ex)
            {
                responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace), 1, 2);
            }

            return responseMessage;
        }

        //http://sdapp003:8048/ModstroemTest/api/v1.0/$metadata

        //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/items
        //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers
        //c29fb1d5-b8ff-4bcf-a075-e8512fa17802

        //[Route("CreateCustomer")]
        //[HttpGet]
        //public HttpResponseMessage CreateCustomer(string ApiKey, string EncryptionKey, string countryLetterCode, string Lbnr, string CVR, string FirmName, string FullName, string Street, string City, string Zip, string Phone, string Email)
        //{
        //    HttpResponseMessage responseMessage = null;

        //    try
        //    {
        //        Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
        //        if (responce.ResponceType != Enums.ResponceType.Ok) { return responseMessage = ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType); }

        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        //        dictionary.Add("ApiKey", ApiKey);
        //        dictionary.Add("EncryptionKey", EncryptionKey);
        //        dictionary.Add("Lbnr", Lbnr);

        //        responce = UtilitiesLibrary.GetInstans().ValidateInput(dictionary);
        //        if (responce.ResponceType != Enums.ResponceType.Ok)
        //        {
        //            return responseMessage = DataTransferObjects.ResponceHandler.Responce("ValidateInput not valid", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType);
        //        }


        //        string existingCustomer = GetCustomer(Lbnr);

        //        if (existingCustomer == "-1")
        //        {
        //            return responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, "Error existingCustomer", "-1"), 1, 2);
        //        }

        //        try
        //        {
        //            if (existingCustomer != "0" && existingCustomer != " ")
        //            {
        //                Guid g = Guid.Parse(existingCustomer);
        //                return responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, "allready existing customer", "-1"), 1, 7);
        //            }
        //        }
        //        catch { }

        //        string currencyCode = "";
        //        if (countryLetterCode == null || countryLetterCode == "" || countryLetterCode == " ")
        //        {
        //            countryLetterCode = "DK";
        //            currencyCode = "DKK";
        //        }

        //        if (CVR == null) { CVR = ""; }
        //        if (FirmName == null) { FirmName = ""; }
        //        if (FullName == null || FullName == "") { FullName = "?"; }
        //        if (Street == null) { Street = ""; }
        //        if (City == null) { City = ""; }
        //        if (Phone == null) { Phone = ""; }
        //        if (Email == null) { Email = ""; }
        //        if (Zip == null) { Zip = ""; }


        //        CVR = CVR.Replace("'", "''");
        //        FirmName = FirmName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        FullName = FullName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Street = Street.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        City = City.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Phone = Phone.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Zip = Zip.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Email = Email.Replace("'", "''");
        //        Lbnr = Lbnr.Replace("'", "''");

        //        if (CVR != "" && CVR != " ") { CVR = CVR.Substring(0, CVR.Length > 20 ? 20 : CVR.Length); }
        //        if (FirmName != "" && FirmName != " ") { FirmName = FirmName.Substring(0, FirmName.Length > 100 ? 100 : FirmName.Length); }
        //        if (FullName != "" && FullName != " ") { FullName = FullName.Substring(0, FullName.Length > 100 ? 100 : FullName.Length); }
        //        if (Lbnr != "" && Lbnr != " ") { Lbnr = Lbnr.Substring(0, Lbnr.Length > 50 ? 50 : Lbnr.Length); } //name 2
        //        if (Street != "" && Street != " ") { Street = Street.Substring(0, Street.Length > 100 ? 100 : Street.Length); }
        //        if (City != "" && City != " ") { City = City.Substring(0, City.Length > 30 ? 30 : City.Length); }
        //        if (Phone != "" && Phone != " ") { Phone = Phone.Substring(0, Phone.Length > 30 ? 30 : Phone.Length); }
        //        if (Zip != "" && Zip != " ") { Zip = Zip.Substring(0, Zip.Length > 20 ? 20 : Zip.Length); }
        //        if (Email != "" && Email != " ") { Email = Email.Substring(0, Email.Length > 80 ? 80 : Email.Length); }

        //        string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers";
        //        string id = "";
        //        try
        //        {

        //            string LastModifiedDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


        //            WebClient client = new WebClient();
        //            client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //            client.Encoding = Encoding.UTF8;
        //            client.Credentials = new NetworkCredential("roman.bolonkin", "123qweASD!19", "akmod");


        //            if (CVR != null && CVR != "" && CVR != " ")
        //            {
        //                var Company = new
        //                {
        //                    type = "Company",
        //                    displayName = FullName + "(" + FirmName + ")",
        //                    phoneNumber = Phone,
        //                    email = Email,
        //                    website = Lbnr,
        //                    currencyCode = currencyCode,
        //                    taxRegistrationNumber = CVR,

        //                    address = new
        //                    {
        //                        street = Street,
        //                        city = City,
        //                        countryLetterCode = countryLetterCode,
        //                        postalCode = Zip
        //                    }
        //                };

        //                var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Company).Replace("\"", @""""));
        //                var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //                var items = jobj.Children().Cast<JProperty>().ToList();

        //                foreach (JProperty p in items)
        //                {
        //                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                }
        //            }
        //            else
        //            {
        //                var Person = new
        //                {
        //                    type = "Person",
        //                    displayName = FullName,
        //                    phoneNumber = Phone,
        //                    email = Email,
        //                    website = Lbnr,
        //                    currencyCode = currencyCode,

        //                    address = new
        //                    {
        //                        street = Street,
        //                        city = City,
        //                        countryLetterCode = countryLetterCode,
        //                        postalCode = Zip
        //                    }
        //                };

        //                var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Person).Replace("\"", @""""));
        //                var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //                var items = jobj.Children().Cast<JProperty>().ToList();

        //                foreach (JProperty p in items)
        //                {
        //                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                }
        //            }

        //            if (id != "")
        //            {
        //                responseMessage = ResponceHandler.Responce("Result data", JsonConvert.SerializeObject(id), 1, 6);
        //            }
        //            else
        //            {
        //                responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR("Error : " + System.Reflection.MethodBase.GetCurrentMethod().Name, "id=null"), 1, 2);
        //            }


        //        }
        //        catch (Exception ex)
        //        {
        //            responseMessage = DataTransferObjects.ResponceHandler.Responce("Exception [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace), 1, 2);
        //    }

        //    return responseMessage;
        //}

        //public string GetCustomerByFilter(string Lbnr)
        //{
        //    string responseMessage = "0";

        //    try
        //    {

        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();//Common Item No_

        //            string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers$filter=website eq '" + Lbnr + "'";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new NetworkCredential("roman.bolonkin", "123qweASD!19", "akmod");

        //            //var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(account));
        //            //var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //            //var items = jobj.Children().Cast<JProperty>().ToList();

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                return responseMessage = "-1";
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;


        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string Lbnr2 = "";//website
        //                        string address = "";

        //                        string street = "";
        //                        string city = "";
        //                        string postalCode = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "website") { Lbnr2 = (string)p.Value; }
        //                            if (p.Name.ToLower() == "phonenumber") { phonenumber = (string)p.Value; }
        //                            if (p.Name.ToLower() == "email") { email = (string)p.Value; }

        //                            if (p.Name.ToLower() == "address")
        //                            {
        //                                try
        //                                {
        //                                    address = p.Value.ToString();
        //                                    var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                                    var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                                    foreach (var d in itemsdata)
        //                                    {
        //                                        if (Lbnr2 != "")
        //                                        {

        //                                        }

        //                                        try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                                    }
        //                                }
        //                                catch { }
        //                            }
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        Customer.Address = new ExpandoObject();
        //                        Customer.Address.PostalCode = postalCode;
        //                        Customer.Address.City = city;
        //                        Customer.Address.Street = street;

        //                        list.Add(Customer);

        //                        //if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        //{
        //                        //    if (Lbnr2 == Lbnr)
        //                        //    {
        //                        //        listSingle.Add(Customer);
        //                        //        breakAll = true;
        //                        //        break;
        //                        //    }
        //                        //}
        //                    }

        //                    //if (breakAll == true)
        //                    //{
        //                    //    break;
        //                    //}
        //                }
        //            }

        //            if (list.Count == 1)
        //            {
        //                dynamic Customer = new ExpandoObject();
        //                Customer = list[0];
        //                responseMessage = Customer.Id;
        //            }
        //            else
        //            {
        //                responseMessage = "";
        //            }


        //        }
        //        catch
        //        {
        //            responseMessage = "-1";
        //        }
        //    }
        //    catch
        //    {
        //        responseMessage = "-1";
        //    }


        //    return responseMessage;
        //}

        //public string GetCustomer(string Lbnr)
        //{
        //    string responseMessage = "0";

        //    try
        //    {

        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();

        //            string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new NetworkCredential(UtilitiesLibrary.GetInstans().userName, UtilitiesLibrary.GetInstans().password, UtilitiesLibrary.GetInstans().domain);

        //            //var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(account));
        //            //var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //            //var items = jobj.Children().Cast<JProperty>().ToList();

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                return responseMessage = "-1";
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;


        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string Lbnr2 = "";//website
        //                        string address = "";

        //                        string street = "";
        //                        string city = "";
        //                        string postalCode = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "website") { Lbnr2 = (string)p.Value; }
        //                            if (p.Name.ToLower() == "phonenumber") { phonenumber = (string)p.Value; }
        //                            if (p.Name.ToLower() == "email") { email = (string)p.Value; }

        //                            if (p.Name.ToLower() == "address")
        //                            {
        //                                try
        //                                {
        //                                    address = p.Value.ToString();
        //                                    var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                                    var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                                    foreach (var d in itemsdata)
        //                                    {
        //                                        if (Lbnr2 != "")
        //                                        {

        //                                        }

        //                                        try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                                    }
        //                                }
        //                                catch { }
        //                            }
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        Customer.Address = new ExpandoObject();
        //                        Customer.Address.PostalCode = postalCode;
        //                        Customer.Address.City = city;
        //                        Customer.Address.Street = street;

        //                        list.Add(Customer);

        //                        if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        {
        //                            if (Lbnr2 == Lbnr)
        //                            {
        //                                listSingle.Add(Customer);
        //                                breakAll = true;
        //                                break;
        //                            }
        //                        }
        //                    }

        //                    if (breakAll == true)
        //                    {
        //                        break;
        //                    }
        //                }
        //            }

        //            if (listSingle.Count == 1)
        //            {
        //                dynamic Customer = new ExpandoObject();
        //                Customer = listSingle[0];
        //                responseMessage = Customer.Id;
        //            }
        //            else
        //            {
        //                responseMessage = "";
        //            }


        //        }
        //        catch
        //        {
        //            responseMessage = "-1";
        //        }
        //    }
        //    catch
        //    {
        //        responseMessage = "-1";
        //    }


        //    return responseMessage;
        //}

        //[Route("GetCustomers")]
        //[HttpGet]
        //public HttpResponseMessage GetCustomers(string ApiKey, string EncryptionKey, string Lbnr)
        //{
        //    HttpResponseMessage responseMessage = null;

        //    try
        //    {
        //        Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
        //        if (responce.ResponceType != Enums.ResponceType.Ok) { return responseMessage = ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType); }

        //        //https://docs.microsoft.com/en-us/dynamics-nav/fin-graph/api/dynamics_customer_get
        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();

        //            //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers?$filter=phoneNumber eq '23275048'

        //            //http://localhost:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers
        //            string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new NetworkCredential(UtilitiesLibrary.GetInstans().userName, UtilitiesLibrary.GetInstans().password, UtilitiesLibrary.GetInstans().domain);

        //            //var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(account));
        //            //var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //            //var items = jobj.Children().Cast<JProperty>().ToList();

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                responseMessage = DataTransferObjects.ResponceHandler.Responce("No data found", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, 5);
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;


        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string Lbnr2 = "";//website
        //                        string address = "";

        //                        string street = "";
        //                        string city = "";
        //                        string postalCode = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "website") { Lbnr2 = (string)p.Value; }
        //                            if (p.Name.ToLower() == "phonenumber") { phonenumber = (string)p.Value; }
        //                            if (p.Name.ToLower() == "email") { email = (string)p.Value; }

        //                            if (p.Name.ToLower() == "address")
        //                            {
        //                                try
        //                                {
        //                                    address = p.Value.ToString();
        //                                    var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                                    var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                                    foreach (var d in itemsdata)
        //                                    {
        //                                        if (Lbnr2 != "")
        //                                        {

        //                                        }

        //                                        try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                                    }
        //                                }
        //                                catch { }
        //                            }
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        Customer.Address = new ExpandoObject();
        //                        Customer.Address.PostalCode = postalCode;
        //                        Customer.Address.City = city;
        //                        Customer.Address.Street = street;

        //                        list.Add(Customer);

        //                        if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        {
        //                            if (Lbnr2 == Lbnr)
        //                            {
        //                                listSingle.Add(Customer);
        //                                breakAll = true;
        //                                break;
        //                            }
        //                        }
        //                    }

        //                    if (breakAll == true)
        //                    {
        //                        break;
        //                    }
        //                }
        //            }

        //            if (listSingle.Count != 0)
        //            {
        //                responseMessage = ResponceHandler.Responce("Result data", JsonConvert.SerializeObject(listSingle), 1, 6);
        //            }
        //            else
        //            {
        //                responseMessage = ResponceHandler.Responce("Result data", JsonConvert.SerializeObject(list), 1, 6);
        //            }


        //        }
        //        catch (Exception ex)
        //        {
        //            responseMessage = ResponceHandler.Responce("Exception [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        responseMessage = ResponceHandler.Responce("Exception General [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
        //    }


        //    return responseMessage;
        //}

        //public string GetProduct(string ProductNr)
        //{
        //    string responseMessage = "0";

        //    try
        //    {

        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();

        //            string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/items";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new NetworkCredential(UtilitiesLibrary.GetInstans().userName, UtilitiesLibrary.GetInstans().password, UtilitiesLibrary.GetInstans().domain);

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                return responseMessage = "-1";
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;

        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string unitprice = "";
        //                        string invintory = "";

        //                        string priceIncludesTax = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "unitprice") { unitprice = (string)p.Value; }
        //                            if (p.Name.ToLower() == "invintory") { invintory = (string)p.Value; }
        //                            if (p.Name.ToLower() == "priceIncludesTax") { priceIncludesTax = (string)p.Value; }

        //                            //if (p.Name.ToLower() == "address")
        //                            //{
        //                            //    try
        //                            //    {
        //                            //        address = p.Value.ToString();
        //                            //        var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                            //        var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                            //        foreach (var d in itemsdata)
        //                            //        {
        //                            //            if (Lbnr2 != "")
        //                            //            {

        //                            //            }

        //                            //            try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                            //            try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                            //            try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                            //        }
        //                            //    }
        //                            //    catch { }
        //                            //}
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        //Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        //Customer.Address = new ExpandoObject();
        //                        //Customer.Address.PostalCode = postalCode;
        //                        //Customer.Address.City = city;
        //                        //Customer.Address.Street = street;

        //                        //list.Add(Customer);

        //                        //if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        //{
        //                        //    if (Lbnr2 == Lbnr)
        //                        //    {
        //                        //        listSingle.Add(Customer);
        //                        //        breakAll = true;
        //                        //        break;
        //                        //    }
        //                        //}
        //                    }

        //                    if (breakAll == true)
        //                    {
        //                        break;
        //                    }
        //                }
        //            }

        //            if (listSingle.Count == 1)
        //            {
        //                dynamic Customer = new ExpandoObject();
        //                Customer = listSingle[0];
        //                responseMessage = Customer.Id;
        //            }
        //            else
        //            {
        //                responseMessage = "";
        //            }


        //        }
        //        catch
        //        {
        //            responseMessage = "-1";
        //        }
        //    }
        //    catch
        //    {
        //        responseMessage = "-1";
        //    }


        //    return responseMessage;
        //}

        //[Route("CreateIvoice")]
        //[HttpGet]
        //public HttpResponseMessage CreateIvoice(string ApiKey, string EncryptionKey, string CustomerId, [FromUri] string[] IL)
        //{
        //    HttpResponseMessage responseMessage = null;
        //    try
        //    {

        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        //        dictionary.Add("ApiKey", ApiKey);
        //        dictionary.Add("EncryptionKey", EncryptionKey);
        //        dictionary.Add("CustomerId", CustomerId);
        //        dictionary.Add("IvoiceLines", IL.ToString());


        //        //Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
        //        //if (responce.ResponceType != Enums.ResponceType.Ok)
        //        //{
        //        //    return responseMessage = DataTransferObjects.ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType);
        //        //}

        //        //responce = UtilitiesLibrary.GetInstans().ValidateInput(dictionary);
        //        //if (responce.ResponceType != Enums.ResponceType.Ok)
        //        //{
        //        //    return responseMessage = DataTransferObjects.ResponceHandler.Responce("ValidateInput not valid", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType);
        //        //}


        //        //try
        //        //{
        //        //    int id = 0;
        //        //    foreach (string str in IL)//Product number
        //        //    {
        //        //        if (id == 2)
        //        //        {
        //        //            SqlDbType foo = (SqlDbType)int.Parse(str);
        //        //            id = 0;
        //        //        }
        //        //        id = id + 1;
        //        //    }


        //        //    List<JObject> list = ExecuteStoredProcedureGetResultMultiParametersDynamicly(StoredProcedureName, PVT);

        //        //    if (list != null && list.Count != 0)
        //        //    {
        //        //        responseMessage = DataTransferObjects.ResponceHandler.Responce("Result", JsonConvert.SerializeObject(list, Formatting.Indented), 1, 6);
        //        //    }
        //        //    else
        //        //    {
        //        //        responseMessage = DataTransferObjects.ResponceHandler.Responce("Exception", JsonConvert.SerializeObject("¤No Result", Formatting.Indented), 1, 8);
        //        //    }

        //        //}
        //        //catch
        //        //{
        //        //    responseMessage = DataTransferObjects.ResponceHandler.Responce("General exception AT [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : Unknown SqlDbType ", 1, 3);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        responseMessage = DataTransferObjects.ResponceHandler.Responce("General exception AT [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
        //    }

        //    return responseMessage;
        //}

        //public string GetProductByFilter(string ProductIdentifier)
        //{
        //    string responseMessage = "0";

        //    try
        //    {

        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();//Common Item No_

        //            string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/items$filter=displayName eq '" + ProductIdentifier + "'";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new NetworkCredential("roman.bolonkin", "123qweASD!19", "akmod");

        //            //var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(account));
        //            //var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //            //var items = jobj.Children().Cast<JProperty>().ToList();

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                return responseMessage = "-1";
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;


        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string Lbnr2 = "";//website
        //                        string address = "";

        //                        string street = "";
        //                        string city = "";
        //                        string postalCode = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "website") { Lbnr2 = (string)p.Value; }
        //                            if (p.Name.ToLower() == "phonenumber") { phonenumber = (string)p.Value; }
        //                            if (p.Name.ToLower() == "email") { email = (string)p.Value; }

        //                            if (p.Name.ToLower() == "address")
        //                            {
        //                                try
        //                                {
        //                                    address = p.Value.ToString();
        //                                    var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                                    var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                                    foreach (var d in itemsdata)
        //                                    {
        //                                        if (Lbnr2 != "")
        //                                        {

        //                                        }

        //                                        try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                                    }
        //                                }
        //                                catch { }
        //                            }
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        Customer.Address = new ExpandoObject();
        //                        Customer.Address.PostalCode = postalCode;
        //                        Customer.Address.City = city;
        //                        Customer.Address.Street = street;

        //                        list.Add(Customer);

        //                        //if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        //{
        //                        //    if (Lbnr2 == Lbnr)
        //                        //    {
        //                        //        listSingle.Add(Customer);
        //                        //        breakAll = true;
        //                        //        break;
        //                        //    }
        //                        //}
        //                    }

        //                    //if (breakAll == true)
        //                    //{
        //                    //    break;
        //                    //}
        //                }
        //            }

        //            if (list.Count == 1)
        //            {
        //                dynamic Customer = new ExpandoObject();
        //                Customer = list[0];
        //                responseMessage = Customer.Id;
        //            }
        //            else
        //            {
        //                responseMessage = "";
        //            }


        //        }
        //        catch
        //        {
        //            responseMessage = "-1";
        //        }
        //    }
        //    catch
        //    {
        //        responseMessage = "-1";
        //    }


        //    return responseMessage;
        //}

        //[Route("CreateProduct")]
        //[HttpGet]
        //public HttpResponseMessage CreateProduct(string ApiKey, string EncryptionKey, string displayName, string type, string itemCategoryId, string itemCategoryCode, string blocked, string baseUnitOfMeasureId, string inventory, string unitPrice, string priceIncludesTax, string unitCost,string baseUnitOfMeasure, string baseUnitOfMeasure_code, string baseUnitOfMeasure_displayName)
        //{
        //    HttpResponseMessage responseMessage = null;

        //    try
        //    {
        //        Responce responce = UtilitiesLibrary.GetInstans().ValidateConnection(ApiKey, EncryptionKey);
        //        if (responce.ResponceType != Enums.ResponceType.Ok) { return responseMessage = ResponceHandler.Responce("ValidateConnection == null", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType); }

        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();
        //        dictionary.Add("ApiKey", ApiKey);
        //        dictionary.Add("EncryptionKey", EncryptionKey);
        //        dictionary.Add("Lbnr", Lbnr);

        //        responce = UtilitiesLibrary.GetInstans().ValidateInput(dictionary);
        //        if (responce.ResponceType != Enums.ResponceType.Ok)
        //        {
        //            return responseMessage = DataTransferObjects.ResponceHandler.Responce("ValidateInput not valid", System.Reflection.MethodBase.GetCurrentMethod().Name, (int)responce.ContainerType, (int)responce.ResponceType);
        //        }


        //        string existingCustomer = GetCustomer(Lbnr);

        //        if (existingCustomer == "-1")
        //        {
        //            return responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, "Error existingCustomer", "-1"), 1, 2);
        //        }

        //        try
        //        {
        //            if (existingCustomer != "0" && existingCustomer != " ")
        //            {
        //                Guid g = Guid.Parse(existingCustomer);
        //                return responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, "allready existing customer", "-1"), 1, 7);
        //            }
        //        }
        //        catch { }

        //        string currencyCode = "";
        //        if (countryLetterCode == null || countryLetterCode == "" || countryLetterCode == " ")
        //        {
        //            countryLetterCode = "DK";
        //            currencyCode = "DKK";
        //        }

        //        if (CVR == null) { CVR = ""; }
        //        if (FirmName == null) { FirmName = ""; }
        //        if (FullName == null || FullName == "") { FullName = "?"; }
        //        if (Street == null) { Street = ""; }
        //        if (City == null) { City = ""; }
        //        if (Phone == null) { Phone = ""; }
        //        if (Email == null) { Email = ""; }
        //        if (Zip == null) { Zip = ""; }


        //        CVR = CVR.Replace("'", "''");
        //        FirmName = FirmName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        FullName = FullName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Street = Street.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        City = City.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Phone = Phone.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Zip = Zip.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
        //        Email = Email.Replace("'", "''");
        //        Lbnr = Lbnr.Replace("'", "''");

        //        if (CVR != "" && CVR != " ") { CVR = CVR.Substring(0, CVR.Length > 20 ? 20 : CVR.Length); }
        //        if (FirmName != "" && FirmName != " ") { FirmName = FirmName.Substring(0, FirmName.Length > 100 ? 100 : FirmName.Length); }
        //        if (FullName != "" && FullName != " ") { FullName = FullName.Substring(0, FullName.Length > 100 ? 100 : FullName.Length); }
        //        if (Lbnr != "" && Lbnr != " ") { Lbnr = Lbnr.Substring(0, Lbnr.Length > 50 ? 50 : Lbnr.Length); } //name 2
        //        if (Street != "" && Street != " ") { Street = Street.Substring(0, Street.Length > 100 ? 100 : Street.Length); }
        //        if (City != "" && City != " ") { City = City.Substring(0, City.Length > 30 ? 30 : City.Length); }
        //        if (Phone != "" && Phone != " ") { Phone = Phone.Substring(0, Phone.Length > 30 ? 30 : Phone.Length); }
        //        if (Zip != "" && Zip != " ") { Zip = Zip.Substring(0, Zip.Length > 20 ? 20 : Zip.Length); }
        //        if (Email != "" && Email != " ") { Email = Email.Substring(0, Email.Length > 80 ? 80 : Email.Length); }

        //        string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers";
        //        string id = "";
        //        try
        //        {

        //            string LastModifiedDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


        //            WebClient client = new WebClient();
        //            client.Headers[HttpRequestHeader.ContentType] = "application/json";
        //            client.Encoding = Encoding.UTF8;
        //            client.Credentials = new NetworkCredential("roman.bolonkin", "123qweASD!19", "akmod");


        //            if (CVR != null && CVR != "" && CVR != " ")
        //            {
        //                var Company = new
        //                {
        //                    type = "Company",
        //                    displayName = FullName + "(" + FirmName + ")",
        //                    phoneNumber = Phone,
        //                    email = Email,
        //                    website = Lbnr,
        //                    currencyCode = currencyCode,
        //                    taxRegistrationNumber = CVR,

        //                    address = new
        //                    {
        //                        street = Street,
        //                        city = City,
        //                        countryLetterCode = countryLetterCode,
        //                        postalCode = Zip
        //                    }
        //                };

        //                var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Company).Replace("\"", @""""));
        //                var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //                var items = jobj.Children().Cast<JProperty>().ToList();

        //                foreach (JProperty p in items)
        //                {
        //                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                }
        //            }
        //            else
        //            {
        //                var Person = new
        //                {
        //                    type = "Person",
        //                    displayName = FullName,
        //                    phoneNumber = Phone,
        //                    email = Email,
        //                    website = Lbnr,
        //                    currencyCode = currencyCode,

        //                    address = new
        //                    {
        //                        street = Street,
        //                        city = City,
        //                        countryLetterCode = countryLetterCode,
        //                        postalCode = Zip
        //                    }
        //                };

        //                var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Person).Replace("\"", @""""));
        //                var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //                var items = jobj.Children().Cast<JProperty>().ToList();

        //                foreach (JProperty p in items)
        //                {
        //                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                }
        //            }

        //            if (id != "")
        //            {
        //                responseMessage = ResponceHandler.Responce("Result data", JsonConvert.SerializeObject(id), 1, 6);
        //            }
        //            else
        //            {
        //                responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR("Error : " + System.Reflection.MethodBase.GetCurrentMethod().Name, "id=null"), 1, 2);
        //            }


        //        }
        //        catch (Exception ex)
        //        {
        //            responseMessage = DataTransferObjects.ResponceHandler.Responce("Exception [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace), 1, 2);
        //    }

        //    return responseMessage;
        //}


    }
}
