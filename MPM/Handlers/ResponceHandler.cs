﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using static MPM.Enums;

namespace MPM.Handlers
{
    public class ResponceHandler
    {
        public ResponceHandler()
        {

        }

        public static HttpResponseMessage Responce(string Message,  string Container, int CT, int RT)
        {
            HttpResponseMessage responseMessage = null;
            Responce responce = new Responce(CT);

            responce.Message = new StringContent(JsonConvert.SerializeObject(Message, Formatting.Indented), Encoding.UTF8, "application/json").ReadAsStringAsync().Result;
            
            responce.Container = Container;

            ContainerType ct = (ContainerType)CT;

            responce.ContainerType = ct;

            ResponceType rt = (ResponceType)RT;
            responce.ResponceType = rt;

            responseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responce, Formatting.Indented), Encoding.UTF8, "application/json"),
            };

            return responseMessage;
        }

        public static HttpResponseMessage Responce(string Message,  string Container, ContainerType CT, ResponceType RT)
        {
            HttpResponseMessage responseMessage = null;
            Responce responce = new Responce((int)CT);

            responce.Message = new StringContent(JsonConvert.SerializeObject(Message, Formatting.Indented), Encoding.UTF8, "application/json").ReadAsStringAsync().Result;
            
            responce.Container = Container;

            ContainerType ct = (ContainerType)CT;

            responce.ContainerType = ct;

            ResponceType rt = (ResponceType)RT;
            responce.ResponceType = rt;

            responseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responce, Formatting.Indented), Encoding.UTF8, "application/json"),
            };

            return responseMessage;
        }

        public static Responce GetResponce(string Message,  string Container, int CT, int RT)
        {
            Responce responce = new Responce(CT);
            responce.Message = new StringContent(JsonConvert.SerializeObject(Message, Formatting.Indented), Encoding.UTF8, "application/json").ReadAsStringAsync().Result;
            
            responce.Container = Container;

            ContainerType ct = (ContainerType)CT;

            responce.ContainerType = ct;

            ResponceType rt = (ResponceType)RT;
            responce.ResponceType = rt;

            return responce;
        }
    }
}