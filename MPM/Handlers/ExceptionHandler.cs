﻿//using GlobalDefs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Globalization;
using System.Text;
using MPM.Authentication;

namespace MPM.Handlers
{
    public class ExceptionHandler
    {
        public static string Message = "";

        public ExceptionHandler()
        {

        }

        public static bool ValidateInternalConnection(string ApiKey, string EncryptionKey)
        {
            AuthenticationController authorisation = new AuthenticationController();

            if (authorisation.Message != "")
            {
                Message += authorisation.Message + " : ValidateConnection";
            }

            bool Access = authorisation.InternalAccessGranted(ApiKey, EncryptionKey);

            if (authorisation.Message != "")
            {
                Message += authorisation.Message + " : ValidateConnection";
            }

            return Access;
        }
        
        public static bool ValidateExternalConnection(string ApiKey, string EncryptionKey)
        {
            AuthenticationController authorisation = new AuthenticationController();

            if (authorisation.Message != "")
            {
                Message += authorisation.Message + " : ValidateConnection";
            }

            bool Access = authorisation.ExternalAccessGranted(ApiKey, EncryptionKey);

            if (authorisation.Message != "")
            {
                Message += authorisation.Message + " : ValidateConnection";
            }

            return Access;
        }

        public static DateTime ValidateDateTimeException(string YYYYMMDD)
        {
            DateTime dt = DateTime.MinValue;

            try
            {
                dt = DateTime.ParseExact(YYYYMMDD, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            catch
            {

            }

            return dt;
        }

        public static DateTime ValidateDateException(string DDMMYYY_HHMMSS)
        {
            DateTime dt = DateTime.MinValue;

            try
            {
                string[] m = DDMMYYY_HHMMSS.Split(' ');
                string[] d = m[0].Split('-');
                string dd = d[0].ToString().PadLeft(2, '0');
                string mm = d[1].ToString().PadLeft(2, '0');
                string yyyy = d[2].ToString();

                string[] h = m[1].Split(':');
                string hh = h[0].ToString().PadLeft(2, '0');
                string mmi = h[1].ToString().PadLeft(2, '0');
                string ss = h[2].ToString().PadLeft(2, '0');

                string date = yyyy + "-" + mm + "-" + dd + "T" + hh + ":" + mmi + ":" + ss;

                dt = DateTime.ParseExact(date, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            }
            catch
            {

            }

            return dt;
        }

        public static DateTime ValidateTimeException(string HHmmss)
        {
            DateTime dt = DateTime.Now.Date;


            try
            {

                string y = String.Format("{0:s}", DateTime.Now);
                string[] m = y.Split('T');
                string t = m[0] + "T" + HHmmss;
                dt = DateTime.Parse(t);
            }
            catch
            {
            }

            return dt;
        }

        public static HttpResponseMessage InternalErrorException(string ApiKey, string ControllerName, string MethodName, string Message)
        {
            HttpResponseMessage responseMessage;
            
            responseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.ExpectationFailed,
                Content = new StringContent(JsonConvert.SerializeObject("", Formatting.Indented), System.Text.Encoding.UTF8, "application/json"),
            };

            return responseMessage;
        }

        public static HttpResponseMessage UnauthorizedException(string ApiKey, string ControllerName, string MethodName)
        {
            HttpResponseMessage responseMessage;
            //DataTransferObjects.WebShop.InformationMessageWebShopDTO im = new DataTransferObjects.WebShop.InformationMessageWebShopDTO();

            //im.Value = "unauthorized";
            //LoggingLibrary.MessageLog.CreateLogEntry(string.Format("Unauthorised access key=" + ApiKey), ControllerName + " " + MethodName, LoggingLibrary.MessageLogType.Besked);
            responseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.Unauthorized,
                Content = new StringContent(JsonConvert.SerializeObject("", Formatting.Indented), System.Text.Encoding.UTF8, "application/json"),
            };

            return responseMessage;
        }

        public static HttpResponseMessage ExceptionError(string MethodName, string ControllerName, Exception ex)
        {

            HttpResponseMessage responseMessage = null;
            Responce responce = new Responce(1);
            responce.Message = "Error -> " + ControllerName + " -> " + MethodName;
            responce.Container = ex.Message + Environment.NewLine + ex.StackTrace;

            responce.ContainerType = Enums.ContainerType.Json;
            responce.ResponceType = Enums.ResponceType.Error;

            responseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(responce, Formatting.Indented), Encoding.UTF8, "application/json"),
            };



            return responseMessage;
        }


    }
}