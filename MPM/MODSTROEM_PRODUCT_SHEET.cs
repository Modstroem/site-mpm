﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MPM
{
    public class MODSTROEM_PRODUCT_SHEET
    {
        public int rowId { get; set; }
        public string PRODUCT_UNIFYINGER_ID { get; set; }
        //public string PRODUCT_OWN_ID { get; set; }
        //public string PRODUCT_NUMBER { get; set; }

        public string ProductNumber { get; set; }
        public string ProductShortName { get; set; }
        public string LastUpdate { get; set; }
        public string ProductType { get; set; }
        public string ProductPrice { get; set; }

        public string Edit { get; set; }


        public MODSTROEM_PRODUCT_SHEET()
        {

        }

    }
}