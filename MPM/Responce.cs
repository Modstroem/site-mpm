﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static MPM.Enums;

namespace MPM
{
    public class Responce
    {
        public string Message { get; set; }

        public string Container { get; set; }
        public ContainerType ContainerType { get; set; }
        public ResponceType ResponceType { get; set; }

        public Responce(int Type = 1) //Defalt ContainerType is JSON = 1
        {
            Message = "";
            Container = "";
            ContainerType ct = (ContainerType)Enum.Parse(typeof(ContainerType), Type.ToString());
            ContainerType = ct;
            ResponceType = ResponceType.Unknown;
        }

        public Responce()
        {
            Message = "";
            Container = "";
            ContainerType ct = ContainerType.Json;
            ContainerType = ct;
            ResponceType = ResponceType.Unknown;
        }
    }
}