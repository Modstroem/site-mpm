﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPM
{
    public class Enums
    {
        public enum WebOrderType
        {
            Unknown = 0,
            RegularProducts = 1,
            ExpensiveProducts = 2,
            ServiceProducts = 3,
            LaborService = 4,
            ElectrissionService = 5,
            InsulationService = 6,
        }

        public enum ResponceType
        {
            Unknown = 0,
            Unauthorized = 1,
            Error = 2,
            InternalServerError = 3,
            ValidationFail = 4,
            Information = 5,
            Ok = 6,
            AlreadyExists = 7,
            NotFound = 8

        }




        public enum DabaseType
        {
            Unknown = 0,
            PROD_DGK = 1,
            TEST_DGK = 2,
            PROD_MODSTROEM = 3,
            TEST_MODSTROEM = 4,
            MODSTROEM_USERS = 5,
            PROD_MEC = 6,
            MIIS = 7,
            PRODUCTS = 7,
        }

        public enum ContainerType
        {
            Unknown = 0,
            Json = 1,
            XML = 2,
        }

        public enum OrderStatus
        {
            New = 1,
            Approved = 2,
            Verified = 3,
            Manuel = 4,
            Warehouse = 5,
            Informed = 6,
            Completed = 7,
            Shipped = 8,
            Received = 9,
            Accounted = 10,
            RollBack = 11,
            PendingForApproval = 12,
            Returned = 13,
            Pending = 14,
            Planned = 15,
            OnHold = 16,
            AwaitingPayment = 17,
            ComplexOrder = 20,
            PreOrder = 21,
            TemporaryState = 70,
            Quarantine = 97,
            UnableToComplete = 90,
            Canceled = 99,
            Error = 100
        }


    }
}