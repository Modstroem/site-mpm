﻿using MPM.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace MPM
{
    public class MUSER
    {
        public List<USER_SETTINGS> ALL_USER_SETTINGS = new List<USER_SETTINGS>();

        public MUSER GetUser(string USERNAME)
        {
            MUSER USER = new MUSER();
            try
            {
                string r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PRODUCTS, "SELECT * FROM [PRODUCTS].[dbo].[USER_SETTINGS] WHERE USERNAME = '@" + USERNAME.ToLower() + "'");
                if (r == "-1") { USER.GetStandartSettings(USERNAME); }
                if (r != null && r != "")
                {
                    string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    // ALL_USER_SETTINGS.Clear();


                    foreach (string u in v)
                    {
                        string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                        USER_SETTINGS U = new USER_SETTINGS();
                        U.USERNAME = m[0].Replace("@", "");
                        U.USER_SETTING_NAME = m[1].Replace("@", "");
                        U.USER_SETTING_VALUE = m[2].Replace("@", "");
                        USER.ALL_USER_SETTINGS.Add(U);
                    }


                }
                else
                {
                    USER.GetStandartSettings(USERNAME);
                    foreach (USER_SETTINGS US in USER.ALL_USER_SETTINGS)
                    {
                        r = UtilitiesLibrary.GetInstans().CreateDataBaseObject(Enums.DabaseType.PRODUCTS, "INSERT INTO [PRODUCTS].[dbo].[USER_SETTINGS]  (USERNAME,USER_SETTING_NAME,USER_SETTING_VALUE) VALUES ('@" + US.USERNAME.ToLower() + "','@" + US.USER_SETTING_NAME + "','@" + US.USER_SETTING_VALUE + "')");
                    }

                    if (r == "-1") { USER.GetStandartSettings(USERNAME); }

                    if (r == "1")
                    {
                        r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PRODUCTS, "SELECT * FROM [PRODUCTS].[dbo].[USER_SETTINGS] WHERE USERNAME = '@" + USERNAME.ToLower() + "'");

                        if (r == "-1") { USER.GetStandartSettings(USERNAME); }

                        if (r != null && r != "")
                        {
                            //ALL_USER_SETTINGS.Clear();


                            string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string u in v)
                            {
                                string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                                USER_SETTINGS U = new USER_SETTINGS();
                                U.USERNAME = m[0].Replace("@", "");
                                U.USER_SETTING_NAME = m[1].Replace("@", "");
                                U.USER_SETTING_VALUE = m[2].Replace("@", "");
                                USER.ALL_USER_SETTINGS.Add(U);

                            }
                        }
                    }
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(ex.Message + " " + ex.StackTrace); }

            return USER;
        }

        public MUSER UpdateUser(string USERNAME, string USER_SETTING_NAME, string USER_SETTING_VALUE)
        {
            MUSER USER = new MUSER();
            try
            {
                // ALL_USER_SETTINGS.Clear();

                string r = UtilitiesLibrary.GetInstans().UpdateDataBaseObject(Enums.DabaseType.PRODUCTS, "UPDATE [PRODUCTS].[dbo].[USER_SETTINGS] SET USER_SETTING_VALUE = '@" + USER_SETTING_VALUE + "' WHERE USERNAME = '@" + USERNAME.ToLower() + "' AND USER_SETTING_NAME = '@" + USER_SETTING_NAME + "'");

                if (r == "-1") { USER.GetStandartSettings(USERNAME); }

                if (r == "1")
                {
                    r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PRODUCTS, "SELECT * FROM [PRODUCTS].[dbo].[USER_SETTINGS] WHERE USERNAME = '@" + USERNAME.ToLower() + "'");

                    if (r == "-1") { USER.GetStandartSettings(USERNAME); }

                    string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string u in v)
                    {
                        string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                        USER_SETTINGS U = new USER_SETTINGS();
                        U.USERNAME = m[0].Replace("@", "");
                        U.USER_SETTING_NAME = m[1].Replace("@", "");
                        U.USER_SETTING_VALUE = m[2].Replace("@", "");
                        USER.ALL_USER_SETTINGS.Add(U);

                    }
                }
                else
                {
                    USER.GetStandartSettings(USERNAME);
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(ex.Message + " " + ex.StackTrace); }

            return USER;
        }

        public List<USER_SETTINGS> GetStandartSettings(string USERNAME)
        {

            USER_SETTINGS USER_SETTING = new USER_SETTINGS();
            USER_SETTING.USERNAME = USERNAME;
            USER_SETTING.USER_SETTING_NAME = "backgroundImage";
            USER_SETTING.USER_SETTING_VALUE = "~/Images/bg3.jpg";
            ALL_USER_SETTINGS.Add(USER_SETTING);
            USER_SETTING = new USER_SETTINGS();
            USER_SETTING.USERNAME = USERNAME;
            USER_SETTING.USER_SETTING_NAME = "backgroundForcolor";
            USER_SETTING.USER_SETTING_VALUE = "Black";
            ALL_USER_SETTINGS.Add(USER_SETTING);
            return ALL_USER_SETTINGS;
        }


    }

    public class USER_SETTINGS
    {

        public string USERNAME { get; set; }
        public string USER_SETTING_NAME { get; set; }
        public string USER_SETTING_VALUE { get; set; }

      

        //public string GetBackgroundImage(string USERNAME)
        //{
        //    string BackgroundImage = "";
        //    try
        //    {
        //        if(ALL_USER_SETTINGS.Count == 0)
        //        {
        //            GetUser(USERNAME);
        //        }
        //        foreach (USER_SETTINGS US in ALL_USER_SETTINGS)
        //        {
        //            if (US.USER_SETTING_NAME == "backgroundImage")
        //            {
        //                BackgroundImage = US.USER_SETTING_VALUE;
        //                break;
        //            }
        //        }

        //    }
        //    catch { BackgroundImage = "-1"; }

        //    return BackgroundImage;
        //}

        //public string SetBackgroundImage()
        //{
        //    string BackgroundImage = "";
        //    try
        //    {
        //        foreach (USER_SETTINGS US in ALL_USER_SETTINGS)
        //        {
        //            if (US.USER_SETTING_NAME == "backgroundImage")
        //            {
        //                BackgroundImage = US.USER_SETTING_VALUE;
        //                break;
        //            }
        //        }

        //    }
        //    catch { BackgroundImage = "-1"; }

        //    return BackgroundImage;
        //}

        //public string GetBackgroundImage()
        //{
        //    string result = "";
        //    try { return Settings.backgroundImage; } catch { result = "-1"; }
        //    return result;
        //}
        //public string SetBackgroundImage(string image)
        //{
        //    string result = "";
        //    try { Settings.backgroundImage = image; } catch { result = "-1"; }
        //    return result;
        //}

        //public string SetBackgroundForcolor(string backgroundForcolor)
        //{
        //    string result = "";
        //    try { Settings.backgroundForcolor = backgroundForcolor; } catch { result = "-1"; }
        //    return result;
        //}

        //public string GetBackgroundForcolor()
        //{
        //    string result = "";
        //    try { return Settings.backgroundForcolor; } catch { result = "-1"; }
        //    return result;
        //}

        //public string GetSETTINGS()
        //{
        //    return SETTINGS.Replace("½", @"""");
        //}

    



    


    }
}