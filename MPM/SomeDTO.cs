﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MPM
{
    public class SomeDTO
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public object SomData { get; set; }
    }
}