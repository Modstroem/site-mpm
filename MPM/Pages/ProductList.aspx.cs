﻿using MPM.Controllers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM.Pages
{
    public partial class ProductList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //BackImageUrl="~/Images/beanstalk.png"
            if (!IsPostBack)
            {
                if (Session["SheetList"] == null)
                {

                    LoadPageRes();
                }
                else
                {
                    try
                    {
                        List<MODSTROEM_PRODUCT_SHEET> sheetList = (List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"];
                        GridView_Products.DataSource = sheetList;
                        GridView_Products.DataBind();
                        // GridStyle(GridView_Products);


                        if (Session["PageIndex"] != null)
                        {
                            try
                            {
                                object o = Session["PageIndexChangingSender"];
                                GridViewPageEventArgs g = (GridViewPageEventArgs)Session["GridViewPageEventArgs"];

                                // GridView_Products_PageIndexChanging(o, g);

                                GridView_Products_PageIndexChanging(GridView_Products, new GridViewPageEventArgs((int)Session["PageIndex"]));
                            }
                            catch { }

                        }

                        if (Session["SelectedIndex"] != null)
                        {
                            GridView_Products.SelectedIndex = Convert.ToInt32(Session["SelectedIndex"]);
                        }

                    }
                    catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
                }
            }
            else
            {
                if (Session["SheetList"] != null)
                {
                    if (this.GridView_Products.Visible == false)
                    {
                        try
                        {
                            List<MODSTROEM_PRODUCT_SHEET> sheetList = (List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"];
                            GridView_Products.DataSource = sheetList;
                            GridView_Products.DataBind();
                            // GridStyle(GridView_Products);
                        }
                        catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

                        Panel2.Visible = true;
                        Label1.Text = "";
                        Image1.ImageUrl = "";
                        Image1.Visible = false;
                        Label1.Visible = false;
                        this.GridView_Products.Visible = true;
                    }
                }
            }

            if (this.DropDownList1.Items.Count == 0)
            {
                //Session["LoadingImage"] = null;
                //Session["SheetList"] = null;
                this.DropDownList1.Items.Clear();
                this.DropDownList1.Items.Add(UtilitiesLibrary.GetInstans().AddEmptyValue());
                ListItem li = new ListItem();
                li.Text = "Nummer";
                li.Value = "2";
                this.DropDownList1.Items.Add(li);
                li = new ListItem();
                li.Text = "Navn";
                li.Value = "3";
                this.DropDownList1.Items.Add(li);
            }

            if (Session["MUSER"] != null)
            {
                try
                {
                    // GridView_Products.BackImageUrl = ((USER_SETTINGS)Session["USER_SETTINGS"]).settings..BACKGROUND_SELECTED;
                }
                catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
            }

            GridStyle(GridView_Products);
        }

        protected void EditButton_Click(object sender, EventArgs e)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            url = url.Replace("ProductList", "CreateProduct.aspx");
            Response.Redirect(url,true);
        }

        public void LoadPageRes()
        {

            if (Session["SheetList"] != null)
            {
                return;
            }
            List<MODSTROEM_PRODUCT> MainList = new List<MODSTROEM_PRODUCT>();
            List<MODSTROEM_PRODUCT_SHEET> sheetList = new List<MODSTROEM_PRODUCT_SHEET>();
            string data = "";
            try
            {
                try
                {
                    NAPI.INSTANCE I = new NAPI.INSTANCE();
                    //var dfg = I.CreateCustomer("120068", "DK78945627", "Kontorcentralen A/S", "Hr. Andreas Kristoffersen", "Carl Blochs Gade 7", "Nyborg", "5800", "23275048", "");
                    //      var dfg = I.Navission_CreateCustomer("20000", "", "", "Roman Bolonkin", "Baltorpvej 30 st. tv.", "Ballerup", "2750", "23275048", "roman@bolonkin.dk");

                   var dfg = I.Navission_CreateProduct("DKG3S1P6002", "Min Cykel", "Inventory", "STK", "True","1350.89", "700", "MOMS25", "");

                    data = I.Navission_GetProducts();
                    JArray Inplist = JArray.Parse(data);

                    if (Inplist != null)
                    {
                        int index = 0;
                        foreach (JObject u in Inplist)
                        {
                            string id = "";
                            string number = "";
                            string displayname = "";
                            string type = "";
                            string baseunitofmeasureid = "";
                            string priceincludestax = "";
                            string unitprice = "";
                            string unitcost = "";
                            string taxgroupid = "";
                            string lastmodifieddatetime = "";
                            string gtin = "?";

                            foreach (JProperty p in u.Properties())
                            {
                                if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                if (p.Name.ToLower() == "displayname") { displayname = (string)p.Value; }
                                if (p.Name.ToLower() == "type") { type = (string)p.Value; }
                                if (p.Name.ToLower() == "baseunitofmeasureid") { baseunitofmeasureid = (string)p.Value; }
                                if (p.Name.ToLower() == "priceincludestax") { priceincludestax = (string)p.Value; }
                                if (p.Name.ToLower() == "unitcost") { unitcost = (string)p.Value; }
                                if (p.Name.ToLower() == "unitprice") { unitprice = (string)p.Value; }
                                if (p.Name.ToLower() == "taxgroupid") { taxgroupid = (string)p.Value; }
                                if (p.Name.ToLower() == "lastmodifieddatetime") { lastmodifieddatetime = (string)p.Value; }
                                if (p.Name.ToLower() == "gtin") { gtin = (string)p.Value; }


                            }

                            if (id != "")
                            {
                                MODSTROEM_PRODUCT_SHEET MPS = new MODSTROEM_PRODUCT_SHEET();
                                MPS.rowId = index++;
                                MPS.PRODUCT_UNIFYINGER_ID = Guid.NewGuid().ToString();
                                MPS.ProductNumber = "DKG1S1P0000";
                                MPS.ProductShortName = displayname;
                                MPS.LastUpdate = "?";
                                try
                                {
                                    //05/22/2019 22:55:55
                                    string YYYY = lastmodifieddatetime[6].ToString() + lastmodifieddatetime[7].ToString() + lastmodifieddatetime[8].ToString() + lastmodifieddatetime[9].ToString();
                                    string MONTH = lastmodifieddatetime[0].ToString() + lastmodifieddatetime[1].ToString();
                                    string DAY = lastmodifieddatetime[3].ToString() + lastmodifieddatetime[4].ToString();

                                    string TIME = lastmodifieddatetime[11].ToString() + lastmodifieddatetime[12].ToString() + lastmodifieddatetime[13].ToString();
                                    TIME += lastmodifieddatetime[14].ToString() + lastmodifieddatetime[15].ToString() + lastmodifieddatetime[16].ToString();
                                    TIME += lastmodifieddatetime[17].ToString() + lastmodifieddatetime[18].ToString();

                                    //lastmodifieddatetime = lastmodifieddatetime.Replace("/", "-").Replace(" ", "T");
                                    MPS.LastUpdate = DateTime.Parse(YYYY + "-" + MONTH + "-" + DAY + "T" + TIME).ToShortDateString();
                                }
                                catch { }

                                MPS.ProductType = type;
                                MPS.ProductPrice = unitprice;
                                MPS.Edit = "¤";
                                sheetList.Add(MPS);

                                MODSTROEM_PRODUCT MP = new MODSTROEM_PRODUCT();

                                MP.PRODUCT_UNIFIER_ID = MPS.PRODUCT_UNIFYINGER_ID;
                                MP.PRODUCT_OWN_ID = id;
                                MP.PRODUCT_NUMBER = MPS.ProductNumber;
                                MP.PRODUCT_SHOT_NAME = displayname;
                                MP.PRODUCT_LONG_NAME = displayname;
                                MP.PRODUCT_EAN_NUMBER = gtin;
                                MP.PRODUCT_DELIVERY_NUMBER = "";
                                MP.PRODUCT_LAST_EDIT = MPS.LastUpdate;


                                MainList.Add(MP);
                            }

                            //ListItem item = new ListItem(displayName, id, true);
                            //dropDownList.Items.Add(item);
                        }
                    }
                }
                catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, data +""+ Environment.NewLine + ex.Message + " " + ex.StackTrace); }


                //int i = 0;
                //foreach (DataRow row in dataTable.Rows)
                //{
                //    try
                //    {
                //        string[] fields = row.ItemArray.Select(field => field.ToString()).ToArray();

                //        OrderHistory oh = new OrderHistory();
                //        oh.SellsInitials = fields[0];
                //        oh.DateFinalized = DateTime.Parse(fields[1]).Date.ToShortDateString().ToString();
                //        oh.OrderNumber = fields[2];
                //        oh.CustomerNumber = fields[3];
                //        oh.CustomerName = fields[4] + " " + fields[5];
                //        oh.PruductName = fields[6];
                //        oh.OrderStatus = fields[8];
                //        oh.WebCrmInitials = "?";
                //        sheetList.Add(oh);
                //    }
                //    catch (Exception ex)
                //    {
                //        Image1.Visible = true;
                //        Image1.ImageUrl = "~/Images/e.gif";
                //    }
                //}

            }
            catch
            {
                Image1.Visible = true;
                Image1.ImageUrl = "~/Images/e.gif";

            }



            if (sheetList.Count != 0)
            {
                Session["SheetList"] = sheetList;
            }


            if (MainList.Count != 0)
            {
                Session["MainList"] = MainList;
            }

            GridView_Products.DataSource = sheetList;
            GridView_Products.DataBind();
            // GridStyle(GridView_Products);



        }

        int rowNumber = 0;
        int PRODUCT_UNIFYINGER_ID = 1;
        int ProductNumber = 2;
        int ProductShortName = 3;
        int LastUpdate = 4;
        int ProductType = 5;
        int ProductPrice = 6;
        int ProductEdit = 7;

        public void GridStyleSetColor(GridView gw)
        {
            try
            {
                //gw.HeaderRow.Cells[rowNumber].Text = "Number";
                //gw.HeaderRow.Cells[PRODUCT_UNIFYINGER_ID].Text = "ID";
                //gw.HeaderRow.Cells[ProductNumber].Text = "Produkt nummer";
                //gw.HeaderRow.Cells[ProductShortName].Text = "Beskrivelse";
                //gw.HeaderRow.Cells[LastUpdate].Text = "Sidst opd.";
                //gw.HeaderRow.Cells[ProductType].Text = "Type";
                //gw.HeaderRow.Cells[ProductPrice].Text = "Salgs Price";
                //gw.HeaderRow.Cells[ProductEdit].Text = "";

                ////gw.HeaderRow.Cells[ProductPrice].ForeColor = c1.BackColor;

                //gw.HeaderRow.Cells[ProductNumber].Width = 80;
                //gw.HeaderRow.Cells[ProductShortName].Width = 180;
                //gw.HeaderRow.Cells[LastUpdate].Width = 80;
                //gw.HeaderRow.Cells[ProductType].Width = 60;
                //gw.HeaderRow.Cells[ProductPrice].Width = 100;
                //gw.HeaderRow.Cells[ProductEdit].Width = 30;

                for (int i = 0; i <= gw.Rows.Count - 1; i++)
                {
                    try
                    {
                        if (Session["MUSER"] != null)
                        {
                            string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                            string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;
                            string USER_SETTING_VALUE = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_VALUE;
                            Color color = Color.FromName(USER_SETTING_VALUE);
                            gw.HeaderRow.Cells[ProductNumber].ForeColor = color;
                            gw.HeaderRow.Cells[ProductShortName].ForeColor = color;
                            gw.HeaderRow.Cells[LastUpdate].ForeColor = color;
                            gw.HeaderRow.Cells[ProductType].ForeColor = color;
                            gw.HeaderRow.Cells[ProductPrice].ForeColor = color;

                            gw.Rows[i].Cells[ProductNumber].ForeColor = color;
                            gw.Rows[i].Cells[ProductShortName].ForeColor = color;
                            gw.Rows[i].Cells[LastUpdate].ForeColor = color;
                            gw.Rows[i].Cells[ProductType].ForeColor = color;
                            gw.Rows[i].Cells[ProductPrice].ForeColor = color;
                            //  gw.Rows[i].Cells[ProductEdit].Width = 30;
                        }
                    }
                    catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

                    gw.Rows[i].Cells[rowNumber].Visible = false;
                    gw.HeaderRow.Cells[rowNumber].Visible = false;

                    gw.Rows[i].Cells[PRODUCT_UNIFYINGER_ID].Visible = false;
                    gw.HeaderRow.Cells[PRODUCT_UNIFYINGER_ID].Visible = false;

                    //gw.HeaderRow.Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
                    //gw.HeaderRow.Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
                    //gw.HeaderRow.Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
                    //gw.HeaderRow.Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
                    //gw.HeaderRow.Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Right;


                    //gw.Rows[i].Cells[ProductNumber].Width = 80;
                    //gw.Rows[i].Cells[ProductShortName].Width = 180;
                    //gw.Rows[i].Cells[LastUpdate].Width = 80;
                    //gw.Rows[i].Cells[ProductType].Width = 60;
                    //gw.Rows[i].Cells[ProductPrice].Width = 100;
                    //gw.Rows[i].Cells[ProductEdit].Width = 30;



                    //try
                    //{
                    //    if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
                    //    {
                    //        if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
                    //        {
                    //            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                    //            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                    //        }
                    //    }

                    //}
                    //catch { }

                    //try
                    //{
                    //    LinkButton Edit = new LinkButton();
                    //    Edit.Text = "Edit";
                    //    Edit.Click += new System.EventHandler(EditButton_Click);
                    //    Edit.Visible = true;
                    //    gw.Rows[i].Cells[ProductEdit].Controls.Add(Edit);
                    //}
                    //catch { }

                    //gw.Rows[i].Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
                    //gw.Rows[i].Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
                    //gw.Rows[i].Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
                    //gw.Rows[i].Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
                    //gw.Rows[i].Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Center;
                    //gw.Rows[i].Cells[ProductEdit].HorizontalAlign = HorizontalAlign.Center;


                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

            // Session["SheetList"] = "0";

            //  Timer1.Enabled = false;

        }

        public void GridStyle(GridView gw)
        {
            try
            {
                gw.HeaderRow.Cells[rowNumber].Text = "Number";
                gw.HeaderRow.Cells[PRODUCT_UNIFYINGER_ID].Text = "ID";
                gw.HeaderRow.Cells[ProductNumber].Text = "Produkt nummer";
                gw.HeaderRow.Cells[ProductShortName].Text = "Beskrivelse";
                gw.HeaderRow.Cells[LastUpdate].Text = "Sidst opd.";
                gw.HeaderRow.Cells[ProductType].Text = "Type";
                gw.HeaderRow.Cells[ProductPrice].Text = "Salgs Price";
                gw.HeaderRow.Cells[ProductEdit].Text = "";

                //gw.HeaderRow.Cells[ProductPrice].ForeColor = c1.BackColor;

                gw.HeaderRow.Cells[ProductNumber].Width = 80;
                gw.HeaderRow.Cells[ProductShortName].Width = 180;
                gw.HeaderRow.Cells[LastUpdate].Width = 80;
                gw.HeaderRow.Cells[ProductType].Width = 60;
                gw.HeaderRow.Cells[ProductPrice].Width = 100;
                gw.HeaderRow.Cells[ProductEdit].Width = 30;

                for (int i = 0; i <= gw.Rows.Count - 1; i++)
                {
                    try
                    {
                        if (Session["MUSER"] != null)
                        {
                            string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                            string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;
                            string USER_SETTING_VALUE = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_VALUE;
                            Color color = Color.FromName(USER_SETTING_VALUE);
                            gw.HeaderRow.Cells[ProductNumber].ForeColor = color;
                            gw.HeaderRow.Cells[ProductShortName].ForeColor = color;
                            gw.HeaderRow.Cells[LastUpdate].ForeColor = color;
                            gw.HeaderRow.Cells[ProductType].ForeColor = color;
                            gw.HeaderRow.Cells[ProductPrice].ForeColor = color;

                            gw.Rows[i].Cells[ProductNumber].ForeColor = color;
                            gw.Rows[i].Cells[ProductShortName].ForeColor = color;
                            gw.Rows[i].Cells[LastUpdate].ForeColor = color;
                            gw.Rows[i].Cells[ProductType].ForeColor = color;
                            gw.Rows[i].Cells[ProductPrice].ForeColor = color;
                          //  gw.Rows[i].Cells[ProductEdit].Width = 30;
                        }
                    }
                    catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

                    gw.Rows[i].Cells[rowNumber].Visible = false;
                    gw.HeaderRow.Cells[rowNumber].Visible = false;

                    gw.Rows[i].Cells[PRODUCT_UNIFYINGER_ID].Visible = false;
                    gw.HeaderRow.Cells[PRODUCT_UNIFYINGER_ID].Visible = false;

                    gw.HeaderRow.Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
                    gw.HeaderRow.Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
                    gw.HeaderRow.Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Right;


                    gw.Rows[i].Cells[ProductNumber].Width = 80;
                    gw.Rows[i].Cells[ProductShortName].Width = 180;
                    gw.Rows[i].Cells[LastUpdate].Width = 80;
                    gw.Rows[i].Cells[ProductType].Width = 60;
                    gw.Rows[i].Cells[ProductPrice].Width = 100;
                    gw.Rows[i].Cells[ProductEdit].Width = 30;



                    //try
                    //{
                    //    if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
                    //    {
                    //        if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
                    //        {
                    //            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
                    //            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
                    //        }
                    //    }

                    //}
                    //catch { }

                    try
                    {
                        LinkButton Edit = new LinkButton();
                        Edit.Text = "Edit";
                        Edit.Click += new System.EventHandler(EditButton_Click);
                        Edit.Visible = true;
                        gw.Rows[i].Cells[ProductEdit].Controls.Add(Edit);
                    }
                    catch { }

                    gw.Rows[i].Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
                    gw.Rows[i].Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Center;
                    gw.Rows[i].Cells[ProductEdit].HorizontalAlign = HorizontalAlign.Center;


                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

            // Session["SheetList"] = "0";

            //  Timer1.Enabled = false;

        }


        //public class Product
        //{
        //    public int rowId { get; set; }
        //    public string PRODUCT_UNIFYINGER_ID { get; set; }
        //    public string PRODUCT_OWN_ID { get; set; }
        //    public string PRODUCT_NUMBER { get; set; }


        //    public string CustomerNumber { get; set; }
        //    public string CustomerName { get; set; }
        //    public string OrderStatus { get; set; }
        //    public string PruductName { get; set; }
        //    public string WebCrmInitials { get; set; }


        //    public Product()
        //    {

        //    }
        //}


        //public class PRODUCTS_MODSTROEM
        //{
        //    public string ID_PRODUCT { get; set; }
        //    public string PRODUCT_NO { get; set; }
        //    public string PRODUCT_SHORT_NAME { get; set; }
        //    public string PRODUCT_NAME { get; set; }
        //    public string PRODUCT_IS_VAT_APPLICABLE { get; set; }
        //    public string ACCOUNTING_IDENTIFICATION { get; set; }
        //    public string PRODUCT_CONTRACT_REQUIRED { get; set; }
        //    public string ID_UNIT { get; set; }
        //    public string ID_VAT { get; set; }
        //    public string ID_AGGREGATION_GROUP { get; set; }
        //    public string BOOL_INVOICED_UNCHANGED { get; set; }
        //}



        //int rowNumber = 0;
        //int ProductNumber = 1;
        //int ProductShortName = 2;
        //int LastUpdate = 3;
        //int ProductType = 4;
        //int ProductPrice = 5;


        //public void GridStyle(GridView gw)
        //{
        //    try
        //    {
        //        gw.HeaderRow.Cells[rowNumber].Text = "Number";
        //        gw.HeaderRow.Cells[ProductNumber].Text = "Produkt nummer";
        //        gw.HeaderRow.Cells[ProductShortName].Text = "Beskrivelse";
        //        gw.HeaderRow.Cells[LastUpdate].Text = "Sidst opd.";
        //        gw.HeaderRow.Cells[ProductType].Text = "Type";
        //        gw.HeaderRow.Cells[ProductPrice].Text = "Salgs Price";

        //        gw.HeaderRow.Cells[ProductNumber].Width = 80;
        //        gw.HeaderRow.Cells[ProductShortName].Width = 180;
        //        gw.HeaderRow.Cells[LastUpdate].Width = 120;
        //        gw.HeaderRow.Cells[ProductType].Width = 60;
        //        gw.HeaderRow.Cells[ProductPrice].Width = 100;

        //        for (int i = 0; i <= gw.Rows.Count - 1; i++)
        //        {


        //            gw.Rows[i].Cells[rowNumber].Visible = false;
        //            gw.HeaderRow.Cells[rowNumber].Visible = false;

        //            gw.HeaderRow.Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
        //            gw.HeaderRow.Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
        //            gw.HeaderRow.Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
        //            gw.HeaderRow.Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
        //            gw.HeaderRow.Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Right;


        //            gw.Rows[i].Cells[ProductNumber].Width = 80;
        //            gw.Rows[i].Cells[ProductShortName].Width = 180;
        //            gw.Rows[i].Cells[LastUpdate].Width = 120;
        //            gw.Rows[i].Cells[ProductType].Width = 60;
        //            gw.Rows[i].Cells[ProductPrice].Width = 100;



        //            //try
        //            //{
        //            //    if (BusinessDaysUntil(DateTime.Parse(gw.Rows[i].Cells[2].Text), DateTime.Now) >= 20)
        //            //    {
        //            //        if (gw.Rows[i].Cells[PruductName].Text.Contains("Afventer kundens accept") == true)
        //            //        {
        //            //            gw.Rows[i].Cells[DateFinalized].ForeColor = Color.Red;
        //            //            gw.Rows[i].Cells[PruductName].ForeColor = Color.Red;
        //            //        }
        //            //    }

        //            //}
        //            //catch { }


        //            gw.Rows[i].Cells[ProductNumber].HorizontalAlign = HorizontalAlign.Left;
        //            gw.Rows[i].Cells[ProductShortName].HorizontalAlign = HorizontalAlign.Left;
        //            gw.Rows[i].Cells[LastUpdate].HorizontalAlign = HorizontalAlign.Center;
        //            gw.Rows[i].Cells[ProductType].HorizontalAlign = HorizontalAlign.Center;
        //            gw.Rows[i].Cells[ProductPrice].HorizontalAlign = HorizontalAlign.Center;


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Session["Error"] += ex.Message + Environment.NewLine + ex.StackTrace;
        //    }

        //    // Session["SheetList"] = "0";

        //    //  Timer1.Enabled = false;

        //}

        protected void GridView_Products_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (!(sender is GridView) || e.Row.RowType != DataControlRowType.DataRow) return;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.background = '#c6cbf2';";
                e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';this.style.background = '';";
                e.Row.ToolTip = "Click to select row";
                e.Row.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink(this.GridView_Products, "Select$" + e.Row.RowIndex);

            }
        }

        protected void GridView_Products_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {


                GridViewRow row = GridView_Products.SelectedRow;
                Session["SelectedRow"] = GridView_Products.SelectedRow.Cells[1].Text;
                Session["SelectedIndex"] = GridView_Products.SelectedIndex;

            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }

        //protected void Timer1_Tick(object sender, EventArgs e)
        //{
        //    if (Session["SheetList"] == null)
        //    {
        //        //Thread thread1 = new Thread(LoadPageRes);
        //        //thread1.IsBackground = true;
        //        //thread1.Start();
        //        //Session["LoadingThread"] = thread1;
        //        LoadPageRes();
        //    }

        //    if (Session["SheetList"] != null)
        //    {
        //        Panel2.Visible = true;

        //        Label1.Text = "";
        //        Image1.ImageUrl = "";
        //        Image1.Visible = false;
        //        Label1.Visible = false;
        //        this.GridView_Products.Visible = true;
        //        Timer1.Enabled = true;
        //    }
        //}



        protected void cmdFind_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (TextBox1.Text.Length > 1)
                {
                    if (Session["SheetList"] != null)
                    {
                        List<MODSTROEM_PRODUCT_SHEET> nList = new List<MODSTROEM_PRODUCT_SHEET>();

                        nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).FindAll(x => x.ProductNumber.ToLower().Contains(TextBox1.Text.ToLower())).ToList();
                        if (nList.Count == 0)
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).FindAll(x => x.ProductShortName.ToLower().Contains(TextBox1.Text.ToLower()));
                        }
                        if (nList.Count == 0)
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).FindAll(x => x.ProductPrice.ToLower().Contains(TextBox1.Text.ToLower()));
                        }

                        GridView_Products.DataSource = nList;
                        GridView_Products.DataBind();
                        GridStyle(GridView_Products);
                    }
                    else
                    {
                        if (Session["SheetList"] != null)
                        {
                            GridView_Products.DataSource = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]);
                            GridView_Products.DataBind();
                            GridStyle(GridView_Products);
                        }
                    }
                }
                else
                {
                    if (Session["SheetList"] != null)
                    {
                        GridView_Products.DataSource = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]);
                        GridView_Products.DataBind();
                        GridStyle(GridView_Products);
                    }
                }

            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["SheetList"] != null)
                {
                    List<MODSTROEM_PRODUCT_SHEET> nList = new List<MODSTROEM_PRODUCT_SHEET>();

                    if (DropDownList1.SelectedItem.Text == "Intet valgt")
                    {
                        nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.rowId).ToList();
                    }
                    if (DropDownList1.SelectedItem.Text == "Nummer")
                    {
                        nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.ProductNumber).ToList();
                    }
                    if (DropDownList1.SelectedItem.Text == "Navn")
                    {
                        nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.ProductShortName).ToList();
                    }
                    GridView_Products.DataSource = nList;
                    GridView_Products.DataBind();
                    GridStyle(GridView_Products);
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            //bool ascending = false;
            //if (ImageButton1.ImageUrl == "~/Images/ascending.png")
            //{
            //    ImageButton1.ImageUrl = "~/Images/descending.png";
            //}
            //if (ImageButton1.ImageUrl == "~/Images/descending.png")
            //{
            //    ImageButton1.ImageUrl = "~/Images/ascending.png";
            //    ascending = true;
            //}

            try
            {
                if (Session["SheetList"] != null)
                {
                    List<MODSTROEM_PRODUCT_SHEET> nList = new List<MODSTROEM_PRODUCT_SHEET>();

                    if (DropDownList1.SelectedItem.Text == "Intet valgt")
                    {
                        if (ImageButton1.ImageUrl == "~/Images/ascending.png")
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.rowId).ToList();
                            ImageButton1.ImageUrl = "~/Images/descending.png";
                        }
                        else
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderByDescending(x => x.rowId).ToList();
                            ImageButton1.ImageUrl = "~/Images/ascending.png";
                        }
                    }
                    if (DropDownList1.SelectedItem.Text == "Nummer")
                    {
                        if (ImageButton1.ImageUrl == "~/Images/ascending.png")
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.ProductNumber).ToList();
                            ImageButton1.ImageUrl = "~/Images/descending.png";
                        }
                        else
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderByDescending(x => x.ProductNumber).ToList();
                            ImageButton1.ImageUrl = "~/Images/ascending.png";
                        }

                    }
                    if (DropDownList1.SelectedItem.Text == "Navn")
                    {
                        if (ImageButton1.ImageUrl == "~/Images/ascending.png")
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderBy(x => x.ProductShortName).ToList();
                            ImageButton1.ImageUrl = "~/Images/descending.png";
                        }
                        else
                        {
                            nList = ((List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"]).OrderByDescending(x => x.ProductShortName).ToList();
                            ImageButton1.ImageUrl = "~/Images/ascending.png";
                        }

                    }
                    GridView_Products.DataSource = nList;
                    GridView_Products.DataBind();
                    GridStyle(GridView_Products);
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }

        protected void cmdChange_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["MUSER"] != null)
            {
                string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundImage").USERNAME;
                string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundImage").USER_SETTING_NAME;
                string USER_SETTING_VALUE = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundImage").USER_SETTING_VALUE;

                if (USER_SETTING_VALUE == "No Image")
                {
                    GridView_Products.BackImageUrl = "~/Images/bg1.jpg";
                    Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.BackImageUrl);
                    return;
                }
                if (USER_SETTING_VALUE == "~/Images/bg1.jpg")
                {
                    GridView_Products.BackImageUrl = "~/Images/bg2.jpg";
                    Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.BackImageUrl);
                    return;
                }
                if (USER_SETTING_VALUE == "~/Images/bg2.jpg")
                {
                    GridView_Products.BackImageUrl = "~/Images/bg3.jpg";
                    Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.BackImageUrl);
                    return;
                }
                if (USER_SETTING_VALUE == "~/Images/bg3.jpg")
                {
                    GridView_Products.BackImageUrl = "No Image";
                    Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.BackImageUrl);
                    return;
                }


                //if (GridView_Products.BackImageUrl == ((USER_SETTINGS)Session["USER_SETTINGS"]).BACKGROUND_SELECTED)
                //{
                //    GridView_Products.BackImageUrl = "~/Images/bg2.jpg";
                //    ChangeSettings();
                //    return;
                //}
                //if (GridView_Products.BackImageUrl == ((USER_SETTINGS)Session["USER_SETTINGS"]).BACKGROUND_SELECTED)
                //{
                //    GridView_Products.BackImageUrl = "~/Images/bg3.jpg";
                //    ChangeSettings();
                //    return;
                //}
                //if (GridView_Products.BackImageUrl == ((USER_SETTINGS)Session["USER_SETTINGS"]).BACKGROUND_SELECTED)
                //{
                //    GridView_Products.BackImageUrl = "No Image";
                //    ChangeSettings();
                //    return;
                //}
                //if (GridView_Products.BackImageUrl == ((USER_SETTINGS)Session["USER_SETTINGS"]).BACKGROUND_SELECTED)
                //{
                //    GridView_Products.BackImageUrl = "~/Images/bg1.jpg";
                //    ChangeSettings();
                //    return;
                //}

            }

        }

        //public void ChangeSettings()
        //{
        //    try
        //    {
        //        if (Session["USER_SETTINGS"] != null)
        //        {
        //            string USERNAME = ((USER_SETTINGS)Session["USER_SETTINGS"]).USERNAME;
        //            string BackImageUrl = GridView_Products.BackImageUrl;
        //            USER_SETTINGS NEW_USER_SETTINGS = ((USER_SETTINGS)Session["USER_SETTINGS"]).UpdateUser(USERNAME, BackImageUrl);
        //            Session["USER_SETTINGS"] = NEW_USER_SETTINGS;
        //        }
        //    }
        //    catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        //}

        protected void cmdChangeTxt_Click(object sender, ImageClickEventArgs e)
        {
            if (c1.Visible == true) { c1.Visible = false; } else { c1.Visible = true; }
            if (c2.Visible == true) { c2.Visible = false; } else { c2.Visible = true; }
            if (c3.Visible == true) { c3.Visible = false; } else { c3.Visible = true; }
            if (c4.Visible == true) { c4.Visible = false; } else { c4.Visible = true; }
        }

        protected void c1_Click(object sender, ImageClickEventArgs e)
        {
            GridView_Products.ForeColor = c1.BackColor;

            if (Session["MUSER"] != null)
            {
                string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;

                Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.ForeColor.Name);
            }

            c1.Visible = false;
            c2.Visible = false;
            c3.Visible = false;
            c4.Visible = false;
            GridStyleSetColor(GridView_Products);
        }

        protected void c2_Click(object sender, ImageClickEventArgs e)
        {
            GridView_Products.ForeColor = c2.BackColor;

            if (Session["MUSER"] != null)
            {
                string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;

                Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.ForeColor.Name);
            }

            c1.Visible = false;
            c2.Visible = false;
            c3.Visible = false;
            c4.Visible = false;
            GridStyleSetColor(GridView_Products);
        }

        protected void c3_Click(object sender, ImageClickEventArgs e)
        {
            GridView_Products.ForeColor = c3.BackColor;

            if (Session["MUSER"] != null)
            {
                string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;

                Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.ForeColor.Name);
            }

            c1.Visible = false;
            c2.Visible = false;
            c3.Visible = false;
            c4.Visible = false;
            GridStyleSetColor(GridView_Products);

        }

        protected void c4_Click(object sender, ImageClickEventArgs e)
        {
            GridView_Products.ForeColor = c4.BackColor;

            if (Session["MUSER"] != null)
            {
                string USERNAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USERNAME;
                string USER_SETTING_NAME = ((MUSER)Session["MUSER"]).ALL_USER_SETTINGS.Find(x => x.USER_SETTING_NAME == "backgroundForcolor").USER_SETTING_NAME;

                Session["MUSER"] = ((MUSER)Session["MUSER"]).UpdateUser(USERNAME, USER_SETTING_NAME, GridView_Products.ForeColor.Name);
            }

            c1.Visible = false;
            c2.Visible = false;
            c3.Visible = false;
            c4.Visible = false;
            GridStyleSetColor(GridView_Products);

        }

        protected void GridView_Products_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Session["PageIndexChangingSender"] = sender;
                Session["GridViewPageEventArgs"] = e;


                if (Session["PageIndex"] != null && Session["SheetList"] != null)
                {
                    GridView gv = (GridView)sender;
                    List<MODSTROEM_PRODUCT_SHEET> sheetList = (List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"];
                    gv.DataSource = sheetList;
                    gv.PageIndex = e.NewPageIndex;
                    gv.DataBind();
                    GridStyle(gv);
                    Session["PageIndex"] = e.NewPageIndex;
                    return;
                }

                if (Session["SheetList"] != null)
                {
                    GridView gv = (GridView)sender;
                    List<MODSTROEM_PRODUCT_SHEET> sheetList = (List<MODSTROEM_PRODUCT_SHEET>)Session["SheetList"];
                    Session["PageIndex"] = e.NewPageIndex;
                    gv.DataSource = sheetList;
                    gv.PageIndex = e.NewPageIndex;
                    gv.DataBind();
                    GridStyle(gv);
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }
    }
}