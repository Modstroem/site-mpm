﻿<%@ Page Title="MPM" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="MPM.Pages.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .mGrid {
            border-collapse: separate;
            border-spacing: 0;
            background-color: #f8f8f8; /*margin: 5px 9px 10px;*/
            padding-left: 20px;
            padding-right: 20px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

            .mGrid td, .mGrid th {
                padding: 8px 5px;
                color: #717171;
                border-top: 1px solid #fff;
                border-bottom: 1px solid #bbb;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid th {
                font-weight: 700;
                border-top: none;
                background: #f8f8f8;
            }

            .mGrid .alt {
            }

            .mGrid .pgr {
            }

                .mGrid .pgr table {
                }

                .mGrid .pgr td {
                    font-weight: 700;
                    color: #ccc;
                    border-bottom: none;
                    background: #f8f8f8;
                }

                    .mGrid .pgr td td {
                        padding: 5px 10px 0 0;
                        border-top: none;
                    }

                .mGrid .pgr a {
                    color: #666;
                    text-decoration: none;
                }

        .auto-style1 {
            width: 303px;
            text-align: left;
        }

        .auto-style2 {
            position: relative;
            top: 8px;
            left: 0px;
        }

        .auto-style3 {
            text-align: right;
        }
        .auto-style4 {
            width: 366px;
            text-align: right;
        }
    </style>

    <div class="jumbotron">
        <asp:Panel ID="Panel2" runat="server" Visible="true">
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox1" AutoPostBack="False" runat="server" AutoCompleteType="Disabled" class="btn btn-default" onclientclick="return false;" Width="250px" style="text-align:left"></asp:TextBox>
                        <asp:ImageButton ID="cmdFind" runat="server" CssClass="auto-style2" ImageUrl="~/Images/iconfinder_system-search_118797.png" OnClick="cmdFind_Click" Width="26px" />
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style3">
                        <asp:ImageButton ID="ImageButton1" runat="server" Height="20px" ImageUrl="~/Images/ascending.png" OnClick="ImageButton1_Click" style="padding-top:-34px;"/>
                       <asp:DropDownList ID="DropDownList1" runat="server" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" Width="165px" AutoPostBack="True" CssClass="btn btn-default" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <asp:Panel ID="Panel1" runat="server" Font-Names="TitilliumWeb-Regular,">
                    &nbsp;<asp:Image ID="Image1" runat="server" BackColor="#EEEEEE" />
                    <asp:Label ID="lblInfo" runat="server" ForeColor="#CC0000"></asp:Label>
                    <asp:Label ID="Label1" runat="server" Text="Vent venligst" Font-Size="Large" Visible="False"></asp:Label>
                    <br />
                    <br />
                    <asp:GridView ID="GridView_Products" runat="server" CssClass="mGrid" OnSelectedIndexChanged="GridView_Products_SelectedIndexChanged" OnRowDataBound="GridView_Products_RowDataBound" Width="100%" AllowPaging="True" AllowSorting="True" Font-Bold="False" ForeColor="#CC0000" OnPageIndexChanging="GridView_Products_PageIndexChanging">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White"
                            HorizontalAlign="Center" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                    <br />

                    <asp:ImageButton ID="cmdChange" runat="server" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/change.jpg" OnClick="cmdChange_Click" />

                    &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="cmdChangeTxt" runat="server" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/Dew.png" OnClick="cmdChangeTxt_Click" />
                    &nbsp;
                    <asp:ImageButton ID="c1" runat="server" BackColor="Black" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/Dew.png" OnClick="c1_Click" Visible="False" />
                    &nbsp;
                    <asp:ImageButton ID="c2" runat="server" BackColor="Blue" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/Dew.png" OnClick="c2_Click" Visible="False" />
                    &nbsp;
                    <asp:ImageButton ID="c3" runat="server" BackColor="Lime" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/Dew.png" OnClick="c3_Click" Visible="False" />
                    &nbsp;
                    <asp:ImageButton ID="c4" runat="server" BackColor="Brown" CssClass="btn btn-default" Height="24px" ImageUrl="~/Images/Dew.png" OnClick="c4_Click" Visible="False" />

                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
