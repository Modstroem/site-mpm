﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateProduct.aspx.cs" Inherits="MPM.Pages.CreateProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .mGrid {
            border-collapse: separate;
            border-spacing: 0;
            background-color: #f8f8f8; /*margin: 5px 9px 10px;*/
            padding-left: 20px;
            padding-right: 20px;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

            .mGrid td, .mGrid th {
                padding: 8px 5px;
                color: #717171;
                border-top: 1px solid #fff;
                border-bottom: 1px solid #bbb;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid tr:hover {
                background: #d3d3d3 url(images/mgrid-hover.png) repeat-x;
            }

            .mGrid th {
                font-weight: 700;
                border-top: none;
                background: #f8f8f8;
            }

            .mGrid .alt {
            }

            .mGrid .pgr {
            }

                .mGrid .pgr table {
                }

                .mGrid .pgr td {
                    font-weight: 700;
                    color: #ccc;
                    border-bottom: none;
                    background: #f8f8f8;
                }

                    .mGrid .pgr td td {
                        padding: 5px 10px 0 0;
                        border-top: none;
                    }

                .mGrid .pgr a {
                    color: #666;
                    text-decoration: none;
                }

        .box {
            display: inline-block;
            vertical-align: top;
            width: 140px;
        }

        .box2 {
            display: inline-block;
            vertical-align: top;
        }

        .auto-style1 {
            width: 36px;
        }

        .auto-style2 {
            height: 3px;
        }

        .auto-style3 {
            text-align: left;
            height: 3px;
        }
    </style>



    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="jumbotron">
                <br />
                <table style="width: 100%;">
                    <tr>
                        <td class="box" rowspan="2">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="auto-style2"></td>
                                    <td class="auto-style3">
                                        <asp:Button ID="Button1" runat="server" class="btn btn-default" OnClick="Button1_Click1" Text="Fælldes" Width="150px" />
                                    </td>
                                    <td class="auto-style2"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left">
                                        <asp:Button ID="Button2" runat="server" class="btn btn-default" OnClick="Button2_Click1" Text="Modstrøm" Width="150px" />
                                        <br />
                                    </td>
                                    <td>&nbsp; &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left">
                                        <asp:Button ID="Button3" runat="server" class="btn btn-default" OnClick="Button3_Click1" Text="Navision" Width="150px" />
                                        <br />
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left">
                                        <asp:Button ID="Button4" runat="server" class="btn btn-default" OnClick="Button4_Click1" Text="DGK" Width="150px" />
                                        <br />
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left">
                                        <asp:Button ID="Button5" runat="server" class="btn btn-default" OnClick="Button5_Click1" Text="OC" Width="150px" />
                                        <br />
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="text-left">
                                        <asp:Button ID="Button6" runat="server" class="btn btn-default" OnClick="Button6_Click1" Text="EB" Width="150px" />
                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px"></td>
                                    <td>&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="height: 1px">
                                        <asp:Button ID="Button7" runat="server" class="btn btn-default" OnClick="Button7_Click" Text="Resultat" Width="150px" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>

                        <td class="auto-style1"></td>
                        <td rowspan="2">
                            <iframe id="myIframe" runat="server" height="800px" name="myIframe" src="~/Pages/SubPages/P_UNIFIER.aspx" frameborder="0" width="100%"></iframe>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="auto-style1">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
