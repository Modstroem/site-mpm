﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="P_UNIFIER.aspx.cs" Inherits="MPM.Pages.SubPages.P_UNIFIER" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body>
   <style type="text/css">
        .auto-style5 {
            width: 198px;
            text-align: center;
        }
        .auto-style12 {
           width: 5px;
       }
       .auto-style14 {
           text-align: right;
       }
       .auto-style15 {
           width: 182px;
       }
       .auto-style16 {
           width: 200px;
           text-align: center;
       }
       .auto-style19 {
           width: 289px;
           text-align: center;
       }
       .auto-style21 {
           width: 5px;
           height: 23px;
       }
       .auto-style22 {
           height: 23px;
       }
       .auto-style23 {
           margin-left: -35px;
       }
    </style>
    <form id="form1" runat="server">
         <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="jumbotron">
                    <table style="width:100%;">
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label1" runat="server"  Text="Produkt numer"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txt" runat="server" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"  class="btn btn-default" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton4" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" OnClick="ImageButton4_Click" />
                                </td>
                        </tr>
                        <tr>
                            <td class="auto-style21"></td>
                            <td colspan="4" class="auto-style22">
                                <asp:Label ID="c" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label2" runat="server" class="btn btn-default" Text="Produkt kort navn"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txt0" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton5" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                               </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c0" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label3" runat="server" class="btn btn-default" Text="Produkt lang navn"></asp:Label>
                            </td>
                            <td class="auto-style5" colspan="2">
                                <asp:TextBox ID="txt1" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" CssClass="auto-style23" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="422px"></asp:TextBox>
                            </td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton3" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton6" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                                </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c1" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label4" runat="server" class="btn btn-default" Text="Leverance varenummer"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txt2" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton8" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton7" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c2" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label5" runat="server" class="btn btn-default" Text="EAN Nummer"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txt3" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton10" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton9" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c3" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label6" runat="server" class="btn btn-default" Text="Leverandør"></asp:Label>
                            </td>
                            <td class="auto-style16">
                                <asp:DropDownList ID="DropDownList1" runat="server" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px">
                                </asp:DropDownList>
                            </td>
                            <td >&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton12" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton11" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c4" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label7" runat="server" class="btn btn-default" Text="?"></asp:Label>
                            </td>
                            <td class="auto-style16">
                                <asp:TextBox ID="txt5" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td class="auto-style14">
                                <asp:ImageButton ID="ImageButton14" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                &nbsp;<asp:ImageButton ID="ImageButton13" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="4">
                                <asp:Label ID="c5" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">
                                <asp:Label ID="Label8" runat="server" class="btn btn-default" Text="?"></asp:Label>
                            </td>
                            <td class="auto-style16">
                                <asp:TextBox ID="txt6" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style19">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                            <td class="auto-style19">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                            <td class="auto-style19">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style15">&nbsp;</td>
                            <td class="auto-style16">&nbsp;</td>
                            <td class="auto-style19">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>  
                <asp:Label ID="lblInfo" runat="server" Font-Underline="True"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
       
    </form>
</body>
</html>
