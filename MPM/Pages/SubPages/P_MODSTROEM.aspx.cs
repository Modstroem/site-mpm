﻿using MPM.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM.Pages.SubPages
{
    public partial class P_MODSTROEM : System.Web.UI.Page
    {

        //        SELECT TOP(1000) [ID_PRODUCT] ,
        //[PRODUCT_NO] ,
        //[PRODUCT_SHORT_NAME] ,
        //[PRODUCT_NAME] ,
        //[PRODUCT_IS_VAT_APPLICABLE] ,
        //[PRODUCT_CONTRACT_REQUIRED] ,
        //[ACCOUNTING_IDENTIFICATION] ,
        //[ID_UNIT] , <- DEF_un dropdown
        //[ID_VAT], 1
        //[ID_PRODUCT_TYPE] ,DEF_PRODUCT_TYPE = låst til 12  dropdown
        //[ID_AGGREGATION_GROUP] , DEF_AGGREGATION_ dropdown
        //[BOOL_INVOICED_UNCHANGED] 1
        //  FROM[MODSTROEM].[dbo].[PRODUCT] where[PRODUCT_NO] like '%DK%' and[ID_PRODUCT_TYPE] = 10

        public class ListItemPosition
        {
            public int possition = 0;
            public List<ListItem> ITEMS = new List<ListItem>();

            public ListItemPosition()
            {

            }
        }

        public class MODSTROEMDATA
        {
            public ListItemPosition DEF_SUPPLIER = new ListItemPosition();
            public ListItemPosition DEF_PRODUCT_UNIT = new ListItemPosition();
            public ListItemPosition DEF_PRODUCT_TYPE = new ListItemPosition();
            public ListItemPosition DEF_AGGREGATION_GROUP = new ListItemPosition();


            public MODSTROEMDATA()
            {
                DEF_SUPPLIER = new ListItemPosition();
                DEF_PRODUCT_UNIT = new ListItemPosition();
                DEF_PRODUCT_TYPE = new ListItemPosition();
                DEF_AGGREGATION_GROUP = new ListItemPosition();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MODSTROEMDATA"] == null)
            {
                LoadData();
            }
            else
            {
                try
                {
                    MODSTROEMDATA data = (MODSTROEMDATA)Session["MODSTROEMDATA"];
                    DropDownList1.Items.Clear();
                    foreach (ListItem li in data.DEF_PRODUCT_UNIT.ITEMS){DropDownList1.Items.Add(li);}
                    DropDownList1.SelectedIndex = data.DEF_PRODUCT_UNIT.possition;

                    DropDownList2.Items.Clear();
                    foreach (ListItem li in data.DEF_PRODUCT_TYPE.ITEMS) { DropDownList2.Items.Add(li); }
                    DropDownList2.SelectedIndex = data.DEF_PRODUCT_TYPE.possition;

                    DropDownList3.Items.Clear();
                    foreach (ListItem li in data.DEF_AGGREGATION_GROUP.ITEMS) { DropDownList3.Items.Add(li); }
                    DropDownList3.SelectedIndex = data.DEF_AGGREGATION_GROUP.possition;

                }
                catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
            }

            if (!IsPostBack)
            {
                //LoadData();
            }
        }

        public void LoadData()
        {
            MODSTROEMDATA data = new MODSTROEMDATA();
            DropDownList1.Items.Clear();
            try
            {
                string r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PROD_MODSTROEM, "SELECT TOP (1000) [ID_UNIT],[UNIT_NAME]FROM[MODSTROEM].[dbo].[DEF_PRODUCT_UNIT]");
                if (r != null && r != "")
                {
                    string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    ListItemPosition p = new ListItemPosition();
                    foreach (string u in v)
                    {
                        string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                        ListItem item = new ListItem(m[1], m[0], true);
                        DropDownList1.Items.Add(item);
                        p.possition = Convert.ToInt32(m[0]);
                        p.ITEMS.Add(item);
                     
                    }
                    data.DEF_PRODUCT_UNIT = p;
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

            DropDownList2.Items.Clear();
            try
            {
                string r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PROD_MODSTROEM, "SELECT TOP (1000) [ID_PRODUCT_TYPE],[PRODUCT_TYPE] FROM[MODSTROEM].[dbo].[DEF_PRODUCT_TYPE]");
                if (r != null && r != "")
                {
                    string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    ListItemPosition p = new ListItemPosition();
                    foreach (string u in v)
                    {
                        string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                        ListItem item = new ListItem(m[1], m[0], true);
                        DropDownList2.Items.Add(item);
                        p.possition = Convert.ToInt32(m[0]);
                        p.ITEMS.Add(item);
                        //  data.DEF_PRODUCT_TYPE.Add(item);

                    }

                    data.DEF_PRODUCT_TYPE = p;
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

            DropDownList3.Items.Clear();
            try
            {
                string r = UtilitiesLibrary.GetInstans().GetResultFromDataBase(Enums.DabaseType.PROD_MODSTROEM, "SELECT TOP (1000) [ID_AGGREGATION_GROUP],[AGGREGATION_GROUP_NAME],[INVOICE_AGGREGATION_NAME]FROM [MODSTROEM].[dbo].[DEF_AGGREGATION_GROUP]");
                if (r != null && r != "")
                {
                    string[] v = r.ToString().Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                    ListItemPosition p = new ListItemPosition();
                    foreach (string u in v)
                    {
                        string[] m = u.ToString().Split(new string[] { "¤" }, StringSplitOptions.RemoveEmptyEntries);
                        ListItem item = new ListItem(m[1], m[0], true);
                        p.possition = Convert.ToInt32(m[0]);
                        p.ITEMS.Add(item);
                        DropDownList3.Items.Add(item);
                    }
                    data.DEF_AGGREGATION_GROUP = p;
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }

            Session["MODSTROEMDATA"] = data;
        }



        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            lblInfo.Text = "bla bla";
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            lblInfo.Text = "Altså det er unike id fra modstrøm.";
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                MODSTROEMDATA data = (MODSTROEMDATA)Session["MODSTROEMDATA"];
                data.DEF_PRODUCT_UNIT.possition = Convert.ToInt32(DropDownList1.SelectedValue);

                //DropDownList2.Items.Clear();
                //foreach (ListItem li in data.DEF_PRODUCT_TYPE.ITEMS) { DropDownList2.Items.Add(li); }
                //DropDownList2.SelectedIndex = data.DEF_PRODUCT_TYPE.possition;

                //DropDownList3.Items.Clear();
                //foreach (ListItem li in data.DEF_AGGREGATION_GROUP.ITEMS) { DropDownList3.Items.Add(li); }
                //DropDownList3.SelectedIndex = data.DEF_AGGREGATION_GROUP.possition;

            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }
    }
}