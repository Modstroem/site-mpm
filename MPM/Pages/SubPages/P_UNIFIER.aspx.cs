﻿using MPM.Controllers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM.Pages.SubPages
{
    public partial class P_UNIFIER : System.Web.UI.Page
    {
        MODSTROEM_PRODUCT MP = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //  this.UpdatePanel1.Update();   
                LoadVendorsData(DropDownList1);
                
            }


            if (Session["MainList"] != null)
            {
                try
                {
                    string PRODUCT_UNIFIER_ID = Session["SelectedRow"].ToString().ToUpper();
                    List<MODSTROEM_PRODUCT> nList = ((List<MODSTROEM_PRODUCT>)Session["MainList"]).FindAll(x => x.PRODUCT_UNIFIER_ID.ToUpper() == PRODUCT_UNIFIER_ID).ToList();
                    if (nList.Count == 1)
                    {
                        MP = nList[0];
                        txt.Text = MP.PRODUCT_NUMBER;
                        txt0.Text = MP.PRODUCT_SHOT_NAME;
                        txt1.Text = MP.PRODUCT_LONG_NAME;
                        txt3.Text = MP.PRODUCT_EAN_NUMBER;

                        
                    }
                       
                }
                catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
            }

            }

        public void LoadVendorsData(DropDownList dropDownList)
        {
            try
            {
                dropDownList.Items.Clear();
                dropDownList.Items.Add(UtilitiesLibrary.GetInstans().AddEmptyValue());
                NAPI.INSTANCE I = new NAPI.INSTANCE();
                JArray Inplist = JArray.Parse(I.Navission_GetVendors());

                if (Inplist != null)
                {
                    foreach (JObject u in Inplist)
                    {
                         List<string> PropertiesFromContainer = UtilitiesLibrary.GetInstans().GetPropertiesFromContainerData(u.ToString());

                        string id = "";
                        string number = "";
                        string displayName = "";

                        foreach (JProperty p in u.Properties())
                        {
                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
                            
                        }

                        ListItem item = new ListItem(displayName, id, true);
                        dropDownList.Items.Add(item);
                    }
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }
        }

    

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            lblInfo.Text = "Dette er produkt numer som bliver brugt som reference i alle systemer.";
        }

        //protected void LinkButton1_Click(object sender, EventArgs e)
        //{
        //    if (LinkButton1.Text == "Ny")
        //    {
        //        LinkButton1.Text = "Gem";
        //        LinkButton2.Visible = true;
        //        txt7.Visible = true;
        //        return;
        //    }

        //    if (LinkButton1.Text == "Gem")
        //    {
        //        LinkButton1.Text = "Ny";
        //        LinkButton2.Visible = false;
        //        txt7.Visible = false;
        //        return;
        //    }
        //}

        //protected void LinkButton2_Click(object sender, EventArgs e)
        //{
        //    LinkButton1.Text = "Ny";
        //    LinkButton2.Visible = false;
        //    txt7.Visible = false;
        //    return;
        //}
    }
}