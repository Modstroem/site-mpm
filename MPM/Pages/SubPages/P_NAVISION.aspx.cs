﻿using MPM.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM.Pages.SubPages
{
    public partial class P_NAVISION : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(c29fb1d5-b8ff-4bcf-a075-e8512fa17802)/vendors
        //vendors
        //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/taxGroups
        public void LoadData()
        {
            DropDownList1.Items.Clear();
            try
            {
                #region items
                List<string> all = new List<string>();
                all.Add("items");
                all.Add("picture");
                all.Add("defaultDimensions");
                all.Add("customers");
                all.Add("customerFinancialDetails");
                all.Add("vendors");
                all.Add("companyInformation");
                all.Add("salesInvoices");
                all.Add("salesInvoiceLines");
                all.Add("pdfDocument");
                all.Add("customerPaymentJournals");
                all.Add("customerPayments");
                all.Add("accounts");
                all.Add("taxGroups");
                all.Add("journals");
                all.Add("journalLines");
                all.Add("attachments");
                all.Add("employees");
                all.Add("timeRegistrationEntries");
                all.Add("generalLedgerEntries");
                all.Add("currencies");
                all.Add("paymentMethods");
                all.Add("dimensions");
                all.Add("dimensionValues");
                all.Add("dimensionLines");
                all.Add("paymentTerms");
                all.Add("shipmentMethods");
                all.Add("itemCategories");
                all.Add("cashFlowStatement");
                all.Add("countriesRegions");
                all.Add("salesOrders");
                all.Add("salesOrderLines");
                all.Add("retainedEarningsStatement");
                all.Add("unitsOfMeasure");
                all.Add("agedAccountsReceivable");
                all.Add("agedAccountsPayable");
                all.Add("balanceSheet");
                all.Add("trialBalance");
                all.Add("incomeStatement");
                all.Add("taxAreas");
                all.Add("salesQuotes");
                all.Add("salesQuoteLines");
                all.Add("salesCreditMemos");
                all.Add("salesCreditMemoLines");
                all.Add("generalLedgerEntryAttachments");
                all.Add("purchaseInvoices");
                all.Add("purchaseInvoiceLines");
                all.Add("customerSales");
                all.Add("vendorPurchases");

                //foreach (string str in all)
                //{
                //    string r2 = UtilitiesLibrary.GetInstans().GetData(@"http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/"+ str, "roman.bolonkin", "ASDqwe123!1");

                //}
                #endregion

                NAPI.INSTANCE I = new NAPI.INSTANCE();
                JArray Inplist = JArray.Parse(I.Navission_GetUnitsOfMeasure());
             
                if (Inplist != null)
                {
                    foreach (JObject u in Inplist)
                    {
                       // List<string> PropertiesFromContainer = UtilitiesLibrary.GetInstans().GetPropertiesFromContainerData(u.ToString());

                        string id = "";
                        string code = "";
                        string displayName = "";
                        string internationalStandardCode = "";
                        string lastModifiedDateTime = "";

                        foreach (JProperty p in u.Properties())
                        {
                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                            if (p.Name.ToLower() == "code") { code = (string)p.Value; }
                            if (p.Name.ToLower() == "displayName") { displayName = (string)p.Value; }
                            if (p.Name.ToLower() == "internationalStandardCode") { internationalStandardCode = (string)p.Value; }
                            if (p.Name.ToLower() == "lastModifiedDateTime") { lastModifiedDateTime = (string)p.Value; }
                        }

                        ListItem item = new ListItem(code, id, true);
                        DropDownList1.Items.Add(item);
                    }
                }
            }
            catch (Exception ex) { UtilitiesLibrary.GetInstans().RegisterError(lblInfo, ex.Message + " " + ex.StackTrace); }


            DropDownList3.Items.Clear();
        }


        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
        {
            lblInfo.Text = "bla bla";
        }

        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {
            lblInfo.Text = "Altså det er unike id fra modstrøm.";
        }

    }
}