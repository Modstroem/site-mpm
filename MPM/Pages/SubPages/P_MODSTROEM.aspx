﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="P_MODSTROEM.aspx.cs" Inherits="MPM.Pages.SubPages.P_MODSTROEM" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
</head>
<body>
   <style type="text/css">
        .auto-style5 {
            width: 198px;
            text-align: center;
        }
        .auto-style12 {
           width: 5px;
       }
       .Seperator {
           height: 3px;
           background-color:darkblue;
       }
       .auto-style17 {
           width: 165px;
       }
       .auto-style18 {
           text-align: right;
       }
    </style>
    <form id="form2" runat="server">
         <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="jumbotron">
                    <table style="width:100%;">
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label3" runat="server"  Text="Produktes egen unik id" class="btn btn-default"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:TextBox ID="txt1" runat="server" BorderColor="#8FECFF" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px"></asp:TextBox>
                            </td>
                            <td class="auto-style18">
                                &nbsp;&nbsp;<asp:ImageButton ID="ImageButton7" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" OnClick="ImageButton7_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="c" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label1" runat="server" Text="Moms inkluderet"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Ja hvis check ind" />
                            </td>
                            <td class="auto-style18">
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton8" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="c0" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label2" runat="server" class="btn btn-default" Text="Strøm kontrakt påkrævet"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox2" runat="server" Text="Ja hvis check ind" />
                            </td>
                             <td class="auto-style18">
                                <asp:ImageButton ID="ImageButton2" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton5" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" OnClick="ImageButton5_Click" />
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="c1" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label4" runat="server" class="btn btn-default" Text="Enhed betegnelse"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:DropDownList ID="DropDownList1" runat="server" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                              <td class="auto-style18">
                                <asp:ImageButton ID="ImageButton3" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton6" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="c2" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label5" runat="server" class="btn btn-default" Text="Produkt type"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:DropDownList ID="DropDownList2" runat="server" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px">
                                </asp:DropDownList>
                            </td>
                             <td class="auto-style18">
                                <asp:ImageButton ID="ImageButton9" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton10" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td colspan="3">
                                <asp:Label ID="c3" runat="server" BackColor="#337AB7" Height="2px" Width="100%"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">
                                <asp:Label ID="Label6" runat="server" class="btn btn-default" Text="Produkt anvenelse"></asp:Label>
                            </td>
                            <td class="auto-style5">
                                <asp:DropDownList ID="DropDownList3" runat="server" BorderStyle="Solid" BorderWidth="1px" class="btn btn-default" onblur="OnBlur(this);" onfocus="OnFocus(this);" Width="165px">
                                </asp:DropDownList>
                            </td>
                              <td class="auto-style18">
                                <asp:ImageButton ID="ImageButton11" runat="server" Height="24px" ImageUrl="~/Images/Logos/warning_48.png" Visible="False" />
                                <asp:ImageButton ID="ImageButton12" runat="server" Height="24px" ImageUrl="~/Images/Logos/info.png" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">&nbsp;</td>
                            <td class="auto-style5">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">&nbsp;</td>
                            <td class="auto-style5">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style12">&nbsp;</td>
                            <td class="auto-style17">&nbsp;</td>
                            <td class="auto-style5">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <asp:Label ID="lblInfo" runat="server"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
