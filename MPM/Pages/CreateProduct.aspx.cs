﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MPM.Pages
{
    public partial class CreateProduct : System.Web.UI.Page
    {

        //https://cooltext.com/Edit-Logo?LogoID=3345104672&3345104672_Gradient1=Frost%20Blue%2005
        //https://cooltext.com/Edit-Logo?LogoID=3345111072&3345111072_Gradient1=Deep%20Sea
        protected void Page_Load(object sender, EventArgs e)
        {
            //if(Session["Button1_Click1"] == null)
            //{
            //    Button1_Click1(null, null);
            //    Session["Button1_Click1"] = "Button1_Click1";
            //}
            if (!IsPostBack)
            {
                Button1_Click1(null, null);

            }


            try
            {
                if (Session["SelectedRow"] != null)
                {

                }
            }
            catch { }
        }


        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {

        }



        ////protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        ////{
        ////    FadeAll();
        ////    img2.ImageUrl = "~/Images/Logos/faelles.png";
        ////    myIframe.Src = "~/Pages/SubPages/P_UNIFIER.aspx";
        ////}

        //protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        //{
        //    //FadeAll();
        //    img2.ImageUrl = "~/Images/Logos/nybo.png";
        //    myIframe.Src = "~/Pages/SubPages/P_MODSTROEM.aspx";

        //}



        //protected void ImageButton3_Click1(object sender, ImageClickEventArgs e)
        //{
        //    //FadeAll();
        //    img2.ImageUrl = "~/Images/Logos/Navision.png";
        //    myIframe.Src = "~/Pages/SubPages/P_NAVISION.aspx";

        //}

        //protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        //{
        //   // FadeAll();
        //    img2.ImageUrl = "~/Images/Logos/eb.png";
        //    myIframe.Src = "~/Pages/SubPages/P_EB.aspx";
        //}

        //protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        //{
        //   // FadeAll();
        //    img2.ImageUrl = "~/Images/Logos/oc.png";
        //    myIframe.Src = "~/Pages/SubPages/P_OC.aspx";

        //}

        //protected void img6_Click(object sender, ImageClickEventArgs e)
        //{
        //    //FadeAll();
        //    img2.ImageUrl = "~/Images/Logos/dgk.png";
        //    myIframe.Src = "~/Pages/SubPages/P_UMBRACO.aspx";
        //}


        public void FadeAll()
        {
            Button1.Font.Bold = false;
            Button2.Font.Bold = false;
            Button3.Font.Bold = false;
            Button4.Font.Bold = false;
            Button5.Font.Bold = false;
            Button6.Font.Bold = false;
            Button7.Font.Bold = false;

            Button1.ForeColor = Color.Black;
            Button2.ForeColor = Color.Black;
            Button3.ForeColor = Color.Black;
            Button4.ForeColor = Color.Black;
            Button5.ForeColor = Color.Black;
            Button6.ForeColor = Color.Black;
            Button7.ForeColor = Color.Black;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button1.Font.Bold = true;
            Button1.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_UNIFIER.aspx";
            //  myIframe.Attributes.Add("src", "~/Pages/SubPages/P_UNIFIER.aspx");
        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button2.Font.Bold = true;
            Button2.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_MODSTROEM.aspx";

        }

        protected void Button3_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button3.Font.Bold = true;
            Button3.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_NAVISION.aspx";

        }

        protected void Button4_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button4.Font.Bold = true;
            Button4.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_UMBRACO.aspx";

        }

        protected void Button5_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button5.Font.Bold = true;
            Button5.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_OC.aspx";

        }

        protected void Button6_Click1(object sender, EventArgs e)
        {
            FadeAll();
            Button6.Font.Bold = true;
            Button6.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_EB.aspx";

        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            FadeAll();
            Button7.Font.Bold = true;
            Button7.ForeColor = Color.MediumBlue;
            myIframe.Src = "~/Pages/SubPages/P_EB.aspx";
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {

        }
    }
}