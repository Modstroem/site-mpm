﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace NAPI
{
    public class INSTANCE
    {
        private string UserName = "roman.bolonkin";
        private string PassWord = "ASDqwe123!2";
        private string Domain = "akmod";
        private string ServerInstands = "http://sdapp003:8048/Modstroemtest/api/v1.0/";
        private string CompanyId = "c29fb1d5-b8ff-4bcf-a075-e8512fa17802";

        //http://sdapp003:8048/Modstroemtest/api/v1.0/companies(c29fb1d5-b8ff-4bcf-a075-e8512fa17802)/customers
        public INSTANCE()
        {

        }

        public Responce ValidateInput(Dictionary<string, string> dictionary)
        {
            Responce responce = new Responce(1);
            try
            {
                var list = dictionary.Where(p => p.Value == null || p.Value == "" || p.Value == " " || p.Value == "  ");

                if (list != null && list.Count() != 0)
                {
                    responce.ResponceType = ResponceType.ValidationFail;
                    foreach (var v in list)
                    {
                        responce.Container += v.Key + " is null or empty.";
                    }
                }
                else
                {
                    responce.Container = "ValidateInput Ok.";
                    responce.ResponceType = ResponceType.Ok;
                }

            }
            catch (Exception ex)
            {
                responce.Message += ex.Message + " " + ex.StackTrace;
                responce.ContainerType = ContainerType.Json;
                responce.ResponceType = ResponceType.InternalServerError;
            }

            if (responce.ResponceType == ResponceType.Unknown)
            {
                if (responce.Message == null || responce.Message == "")
                {
                    responce.ContainerType = ContainerType.Json;
                    responce.Message = "Please contact developer team";
                }
            }

            return responce;
        }

        //public Responce Install(string UserName, string PassWord, string Domain, string CompanyId, string ServerInstands)
        //{
        //    Responce responce = new Responce(1);

        //    try
        //    {
        //        Dictionary<string, string> dictionary = new Dictionary<string, string>();

        //        dictionary.Add(nameof(UserName), UserName);
        //        dictionary.Add(nameof(PassWord), PassWord);
        //        //dictionary.Add(nameof(Domain), Domain);
        //        dictionary.Add(nameof(CompanyId), CompanyId);
        //        dictionary.Add(nameof(ServerInstands), ServerInstands);

        //        responce = ValidateInput(dictionary);
        //        if (responce.ResponceType != ResponceType.Ok)
        //        {
        //            responce.ResponceType = ResponceType.ValidationFail;
        //            responce.Message = "ValidateInput not valid" + System.Reflection.MethodBase.GetCurrentMethod().Name;
        //            responce.Container = "";
        //            return responce;
        //        }

        //        this.UserName = UserName;
        //        this.PassWord = PassWord;
        //        this.Domain = Domain;
        //        this.CompanyId = CompanyId;
        //        this.ServerInstands = ServerInstands;
        //        List<string> list = new List<string>();
        //        list.Add("id");
        //        list.Add("systemVersion");
        //        list.Add("name");
        //        list.Add("displayName");
        //        list.Add("id");
        //        string info = GetNavissionTest();
        //        responce.ResponceType = ResponceType.Ok;
        //        responce.Message = "All good";
        //        responce.Container = info;

        //    }
        //    catch (Exception ex)
        //    {
        //        responce.ResponceType = ResponceType.InternalServerError;
        //        responce.Message = "Exception: " + ex.Message + " - " + ex.StackTrace;
        //        responce.Container = "";
        //    }

        //    return responce;
        //}

        public string CleanData(dynamic data)
        {
            string Data = "";
            try
            {
                Data = data.ToString();
                Data = Data.Replace(@"""value"": [", "");
                Data = Data.Remove(0, 4);
                Data = Data.Remove(Data.Length - 3, 3);
                Data = Data.Replace(@"]", "");
                Data = Data.Insert(0, "[" + Environment.NewLine);
                Data += "]" + Environment.NewLine;
            }
            catch { }

            return Data;
        }

        private List<string> GetPropertiesFromObject(string Data)
        {
            List<string> PropertiesFromMetaData = new List<string>();
            try
            {
                JObject dynamicResult = JObject.Parse(JsonConvert.DeserializeObject(Data).ToString());

                foreach (var item in dynamicResult)
                {
                    PropertiesFromMetaData.Add(item.Key.ToString());
                }

            }
            catch { }

            return PropertiesFromMetaData;
        }

        public string Navission_GetUnitsOfMeasureId(string MeasureCode)
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("unitsOfMeasure", "id", "code", "displayName", "internationalStandardCode", "lastModifiedDateTime");

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;
                    bool FullBreak = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string code = "";
                                string displayName = "";
                                string internationalStandardCode = "";
                                string lastModifiedDateTime = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "code") { code = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayName") { displayName = (string)p.Value; }
                                    if (p.Name.ToLower() == "internationalStandardCode") { internationalStandardCode = (string)p.Value; }
                                    if (p.Name.ToLower() == "lastModifiedDateTime") { lastModifiedDateTime = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    dynamic UnitsOfMeasure = new ExpandoObject();
                                    UnitsOfMeasure.Id = id;
                                    UnitsOfMeasure.code = code;
                                    UnitsOfMeasure.displayName = displayName;
                                    UnitsOfMeasure.internationalStandardCode = internationalStandardCode;
                                    UnitsOfMeasure.lastModifiedDateTime = lastModifiedDateTime;
                                    //list.Add(UnitsOfMeasure);
                                }

                                if (MeasureCode != "")
                                {
                                    if (MeasureCode.ToLower() == code.ToLower())
                                    {
                                        data = id;
                                        FullBreak = true;
                                        break;
                                    }
                                }

                            }
                        }
                        if (FullBreak == true)
                        {
                            break;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string Navission_GetTaxGroupId(string TaxGroupName)
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("taxGroups", "id", "code", "displayName", "taxType", "lastModifiedDateTime");

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;
                    bool FullBreak = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string code = "";
                                string displayName = "";
                                string taxType = "";
                                string lastModifiedDateTime = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "code") { code = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayName") { displayName = (string)p.Value; }
                                    if (p.Name.ToLower() == "lastmodifieddatetime") { lastModifiedDateTime = (string)p.Value; }
                                    if (p.Name.ToLower() == "taxtype") { taxType = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    dynamic UnitsOfMeasure = new ExpandoObject();
                                    UnitsOfMeasure.Id = id;
                                    UnitsOfMeasure.code = code;
                                    UnitsOfMeasure.displayName = displayName;
                                    UnitsOfMeasure.lastModifiedDateTime = lastModifiedDateTime;
                                    UnitsOfMeasure.taxType = taxType;
                                   //list.Add(UnitsOfMeasure);
                                }

                                if (TaxGroupName != "")
                                {
                                    if (TaxGroupName.ToLower() == code.ToLower())
                                    {
                                        data = id;
                                        FullBreak = true;
                                        break;
                                    }
                                }

                            }
                        }
                        if (FullBreak == true)
                        {
                            break;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string Navission_GetUnitsOfMeasure()
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("unitsOfMeasure", "id", "code", "displayName", "internationalStandardCode", "lastModifiedDateTime");

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string code = "";
                                string displayName = "";
                                string internationalStandardCode = "";
                                string lastModifiedDateTime = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "code") { code = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayName") { displayName = (string)p.Value; }
                                    if (p.Name.ToLower() == "internationalStandardCode") { internationalStandardCode = (string)p.Value; }
                                    if (p.Name.ToLower() == "lastModifiedDateTime") { lastModifiedDateTime = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    dynamic UnitsOfMeasure = new ExpandoObject();
                                    UnitsOfMeasure.Id = id;
                                    UnitsOfMeasure.code = code;
                                    UnitsOfMeasure.displayName = displayName;
                                    UnitsOfMeasure.internationalStandardCode = internationalStandardCode;
                                    UnitsOfMeasure.lastModifiedDateTime = lastModifiedDateTime;
                                    list.Add(UnitsOfMeasure);
                                }

                            }
                        }
                    }
                }

                data = JsonConvert.SerializeObject(list);

            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string Navission_GetVendors()
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("vendors", "id", "number", "displayName");

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string number = "";
                                string displayName = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    dynamic UnitsOfMeasure = new ExpandoObject();
                                    UnitsOfMeasure.Id = id;
                                    UnitsOfMeasure.number = number;
                                    UnitsOfMeasure.displayName = displayName;

                                    list.Add(UnitsOfMeasure);
                                }

                            }
                        }
                    }
                }

                data = JsonConvert.SerializeObject(list);

            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string Navission_GetProduct(string productNumber)
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("items", "id", "number", "displayName", "type", "baseUnitOfMeasureId", "priceIncludesTax", "unitPrice", "unitCost", "taxGroupId", "lastModifiedDateTime", "gtin");

                if (data.Contains("The remote server returned an error: (401) Unauthorized."))
                {
                    //UtilitiesLibrary.GetInstans().RegisterError(lblInfo, items);
                    return data;
                }

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;
                    bool fulStop = false;
                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string number = "";
                                string displayname = "";
                                string type = "";
                                string baseunitofmeasureid = "";
                                string priceincludestax = "";
                                string unitprice = "";
                                string unitcost = "";
                                string taxgroupid = "";
                                string lastmodifieddatetime = "";
                                string gtin = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayname") { displayname = (string)p.Value; }
                                    if (p.Name.ToLower() == "type") { type = (string)p.Value; }
                                    if (p.Name.ToLower() == "baseunitofmeasureid") { baseunitofmeasureid = (string)p.Value; }
                                    if (p.Name.ToLower() == "priceincludestax") { priceincludestax = (string)p.Value; }
                                    if (p.Name.ToLower() == "unitprice") { unitprice = (string)p.Value; }
                                    if (p.Name.ToLower() == "unitprice") { unitcost = (string)p.Value; }
                                    if (p.Name.ToLower() == "taxgroupid") { taxgroupid = (string)p.Value; }
                                    if (p.Name.ToLower() == "lastmodifieddatetime") { lastmodifieddatetime = (string)p.Value; }
                                    if (p.Name.ToLower() == "gtin") { gtin = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    if (number.ToLower() == productNumber.ToLower())
                                    {
                                        if (id != "")
                                        {
                                            dynamic UnitsOfMeasure = new ExpandoObject();
                                            UnitsOfMeasure.Id = id;
                                            UnitsOfMeasure.number = number;
                                            UnitsOfMeasure.displayName = displayname;
                                            UnitsOfMeasure.type = type;
                                            UnitsOfMeasure.baseunitofmeasureid = baseunitofmeasureid;
                                            UnitsOfMeasure.priceincludestax = priceincludestax;
                                            UnitsOfMeasure.unitcost = unitcost;
                                            UnitsOfMeasure.taxgroupid = taxgroupid;
                                            UnitsOfMeasure.unitprice = unitprice;
                                            UnitsOfMeasure.lastmodifieddatetime = lastmodifieddatetime;
                                            UnitsOfMeasure.gtin = gtin;
                                            list.Add(UnitsOfMeasure);
                                            fulStop = true;
                                            break;
                                        }
                                    }
                                }

                             
                                   

                            }
                        }

                        if (fulStop == true) { break; }
                    }
                }

                data = JsonConvert.SerializeObject(list);

            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string Navission_GetProducts()
        {
            string data = "";

            try
            {
                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("items", "id", "number", "displayName", "type", "baseUnitOfMeasureId", "priceIncludesTax", "unitPrice", "unitCost", "taxGroupId", "lastModifiedDateTime", "gtin");

                if (data.Contains("The remote server returned an error: (401) Unauthorized."))
                {
                    //UtilitiesLibrary.GetInstans().RegisterError(lblInfo, items);
                    return data;
                }

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;
                    bool fulStop = false;
                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string number = "";
                                string displayname = "";
                                string type = "";
                                string baseunitofmeasureid = "";
                                string priceincludestax = "";
                                string unitprice = "";
                                string unitcost = "";
                                string taxgroupid = "";
                                string lastmodifieddatetime = "";
                                string gtin = "";

                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayname") { displayname = (string)p.Value; }
                                    if (p.Name.ToLower() == "type") { type = (string)p.Value; }
                                    if (p.Name.ToLower() == "baseunitofmeasureid") { baseunitofmeasureid = (string)p.Value; }
                                    if (p.Name.ToLower() == "priceincludestax") { priceincludestax = (string)p.Value; }
                                    if (p.Name.ToLower() == "unitprice") { unitprice = (string)p.Value; }
                                    if (p.Name.ToLower() == "unitprice") { unitcost = (string)p.Value; }
                                    if (p.Name.ToLower() == "taxgroupid") { taxgroupid = (string)p.Value; }
                                    if (p.Name.ToLower() == "lastmodifieddatetime") { lastmodifieddatetime = (string)p.Value; }
                                    if (p.Name.ToLower() == "gtin") { gtin = (string)p.Value; }
                                }

                                if (id != "")
                                {
                                    dynamic UnitsOfMeasure = new ExpandoObject();
                                    UnitsOfMeasure.Id = id;
                                    UnitsOfMeasure.number = number;
                                    UnitsOfMeasure.displayName = displayname;
                                    UnitsOfMeasure.type = type;
                                    UnitsOfMeasure.baseunitofmeasureid = baseunitofmeasureid;
                                    UnitsOfMeasure.priceincludestax = priceincludestax;
                                    UnitsOfMeasure.unitcost = unitcost;
                                    UnitsOfMeasure.taxgroupid = taxgroupid;
                                    UnitsOfMeasure.unitprice = unitprice;
                                    UnitsOfMeasure.lastmodifieddatetime = lastmodifieddatetime;
                                    UnitsOfMeasure.gtin = gtin;
                                    list.Add(UnitsOfMeasure);
                                }
                            }
                        }

                        if (fulStop == true) { break; }
                    }
                }

                data = JsonConvert.SerializeObject(list);

            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = reader.ToString();
                }
            }

            return data;
        }

        public string GetNavissionData(string entityName, string property1 = "", string property2 = "", string property3 = "", string property4 = "", string property5 = ""
            , string property6 = "", string property7 = "", string property8 = "", string property9 = "", string property10 = "", string property11 = "", string property12 = ""
            , string property13 = "", string property14 = "", string property15 = "", string property16 = "", string property17 = "", string property18 = "", string property19 = "")
        {
            string data = "";

            try
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");

                List<ExpandoObject> list = new List<ExpandoObject>();
                string url = ServerInstands + "companies(" + CompanyId + ")/" + entityName;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Credentials = new System.Net.NetworkCredential(UserName, PassWord);
                httpWebRequest.Timeout = 7200000;
                httpWebRequest.ReadWriteTimeout = 7200000;
                httpWebRequest.ServicePoint.MaxIdleTime = 7200000;
                WebResponse response = httpWebRequest.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    data = reader.ReadToEnd();
                }
                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);


                    bool first = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        string Data = obj.ToString();
                        Data = Data.Replace(@"""value"": [", "");
                        Data = Data.Remove(0, 4);
                        Data = Data.Remove(Data.Length - 3, 3);
                        Data = Data.Replace(@"]", "");
                        Data = Data.Insert(0, "[" + Environment.NewLine);
                        Data += "]" + Environment.NewLine;


                        JArray Inplist = JArray.Parse(Data);
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == nameof(property1)) { property1 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property2)) { property2 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property3)) { property3 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property4)) { property4 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property5)) { property5 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property6)) { property6 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property7)) { property7 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property8)) { property8 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property9)) { property9 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property10)) { property10 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property11)) { property11 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property12)) { property12 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property13)) { property13 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property14)) { property14 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property15)) { property15 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property16)) { property16 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property17)) { property17 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property18)) { property18 = (string)p.Value; }
                                    if (p.Name.ToLower() == nameof(property19)) { property19 = (string)p.Value; }


                                }

                                dynamic Unit = new ExpandoObject();

                                Unit.property1 = property1;
                                Unit.property2 = property2;
                                Unit.property3 = property3;
                                Unit.property4 = property4;
                                Unit.property5 = property5;
                                Unit.property6 = property6;
                                Unit.property7 = property7;
                                Unit.property8 = property8;
                                Unit.property9 = property9;
                                Unit.property10 = property10;
                                Unit.property11 = property11;
                                Unit.property12 = property12;
                                Unit.property13 = property13;
                                Unit.property14 = property14;
                                Unit.property15 = property15;
                                Unit.property16 = property16;
                                Unit.property17 = property17;
                                Unit.property18 = property18;
                                Unit.property19 = property19;


                                list.Add(Unit);

                            }
                        }
                    }
                }



            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    data = ex.Message.ToString();
                }
            }

            return data;
        }

        //public string GetCustomerByFilter(string Lbnr)
        //{
        //    string responseMessage = "0";

        //    try
        //    {

        //        try
        //        {
        //            string result = "";
        //            List<ExpandoObject> list = new List<ExpandoObject>();
        //            List<ExpandoObject> listSingle = new List<ExpandoObject>();//Common Item No_

        //           string url = "http://sdapp003:8048/Modstroemtest/api/v1.0/companies(76df632d-b8b9-43c5-8333-4ca813618242)/customers$filter=website eq '" + Lbnr + "'";

        //            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //            request.Timeout = 7200000;
        //            request.ReadWriteTimeout = 7200000;
        //            request.ServicePoint.MaxIdleTime = 7200000;
        //            request.Credentials = new System.Net.NetworkCredential(UserName, PassWord);


        //            //var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(account));
        //            //var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
        //            //var items = jobj.Children().Cast<JProperty>().ToList();

        //            try
        //            {

        //                WebResponse response = request.GetResponse();

        //                using (Stream responseStream = response.GetResponseStream())
        //                {
        //                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
        //                    result = reader.ReadToEnd();
        //                }
        //            }
        //            catch (WebException ex)
        //            {
        //                if (ex.Response != null)
        //                {
        //                    using (var errorResponse = (HttpWebResponse)ex.Response)
        //                    {
        //                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
        //                        {
        //                            string error = reader.ReadToEnd();
        //                            if (error.Contains("ERR_NOT_FOUND"))
        //                            {
        //                                return responseMessage = "-1";
        //                            }

        //                        }
        //                    }
        //                }
        //            }

        //            dynamic DeserializeObjects = JsonConvert.DeserializeObject(result);
        //            bool first = false;
        //            bool breakAll = false;
        //            foreach (var obj in DeserializeObjects)
        //            {
        //                if (first == false) { first = true; continue; }

        //                string Data = obj.ToString();
        //                Data = Data.Replace(@"""value"": [", "");
        //                Data = Data.Remove(0, 4);
        //                Data = Data.Remove(Data.Length - 3, 3);
        //                Data = Data.Replace(@"]", "");
        //                Data = Data.Insert(0, "[" + Environment.NewLine);
        //                Data += "]" + Environment.NewLine;


        //                JArray Inplist = JArray.Parse(Data);
        //                if (Inplist != null)
        //                {
        //                    foreach (JObject u in Inplist)
        //                    {
        //                        string id = "";
        //                        string displayName = "";
        //                        string email = "";
        //                        string phonenumber = "";
        //                        string type = "";
        //                        string number = "";
        //                        string Lbnr2 = "";//website
        //                        string address = "";

        //                        string street = "";
        //                        string city = "";
        //                        string postalCode = "";

        //                        foreach (JProperty p in u.Properties())
        //                        {
        //                            if (p.Name.ToLower() == "id") { id = (string)p.Value; }
        //                            if (p.Name.ToLower() == "displayname") { displayName = (string)p.Value; }
        //                            if (p.Name.ToLower() == "number") { number = (string)p.Value; }
        //                            if (p.Name.ToLower() == "type") { type = (string)p.Value; }
        //                            if (p.Name.ToLower() == "website") { Lbnr2 = (string)p.Value; }
        //                            if (p.Name.ToLower() == "phonenumber") { phonenumber = (string)p.Value; }
        //                            if (p.Name.ToLower() == "email") { email = (string)p.Value; }

        //                            if (p.Name.ToLower() == "address")
        //                            {
        //                                try
        //                                {
        //                                    address = p.Value.ToString();
        //                                    var jobjdata = (JObject)JsonConvert.DeserializeObject(address);
        //                                    var itemsdata = jobjdata.Children().Cast<JProperty>().ToList();
        //                                    foreach (var d in itemsdata)
        //                                    {
        //                                        if (Lbnr2 != "")
        //                                        {

        //                                        }

        //                                        try { if (d.Name.ToLower() == "street") { street = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "city") { city = (string)d.Value; } } catch { }
        //                                        try { if (d.Name.ToLower() == "postalcode") { postalCode = (string)d.Value; } } catch { }
        //                                    }
        //                                }
        //                                catch { }
        //                            }
        //                        }

        //                        dynamic Customer = new ExpandoObject();

        //                        Customer.Id = id;
        //                        Customer.Lbnr = Lbnr2;
        //                        Customer.CustomerType = type;
        //                        Customer.Fullname = displayName;
        //                        Customer.PhoneNumber = phonenumber;
        //                        Customer.Email = email;
        //                        Customer.Fullname = displayName;
        //                        Customer.Address = new ExpandoObject();
        //                        Customer.Address.PostalCode = postalCode;
        //                        Customer.Address.City = city;
        //                        Customer.Address.Street = street;

        //                        list.Add(Customer);

        //                        //if (Lbnr != null && Lbnr != "" && Lbnr != " ")
        //                        //{
        //                        //    if (Lbnr2 == Lbnr)
        //                        //    {
        //                        //        listSingle.Add(Customer);
        //                        //        breakAll = true;
        //                        //        break;
        //                        //    }
        //                        //}
        //                    }

        //                    //if (breakAll == true)
        //                    //{
        //                    //    break;
        //                    //}
        //                }
        //            }

        //            if (list.Count == 1)
        //            {
        //                dynamic Customer = new ExpandoObject();
        //                Customer = list[0];
        //                responseMessage = Customer.Id;
        //            }
        //            else
        //            {
        //                responseMessage = "";
        //            }


        //        }
        //        catch
        //        {
        //            responseMessage = "-1";
        //        }
        //    }
        //    catch
        //    {
        //        responseMessage = "-1";
        //    }


        //    return responseMessage;
        //}

        public string Navission_CreateCustomer(string Lbnr, string CVR, string FirmName, string FullName, string Street, string City, string Zip, string Phone, string Email)
        {
            string data = "";
            string Userid = "";
            string entityName = "customers";
            try
            {

                List<ExpandoObject> list = new List<ExpandoObject>();
                data = GetNavissionData("customers", "id", "number", "displayName", "type", "email", "website", "taxAreaId", "taxAreaDisplayName", "taxRegistrationNumber", "lastModifiedDateTime", "currencyId", "currencyCode", "paymentTermsId", "shipmentMethodId", "paymentMethodId", "blocked", "lastModifiedDateTime");

                if (data.Contains("The remote server returned an error: (401) Unauthorized."))
                {
                    //UtilitiesLibrary.GetInstans().RegisterError(lblInfo, items);
                    return Userid = "-1";
                }

                if (data != null && data != "")
                {
                    dynamic DeserializeObjects = JsonConvert.DeserializeObject(data);
                    bool first = false;

                    foreach (var obj in DeserializeObjects)
                    {
                        if (first == false) { first = true; continue; }

                        JArray Inplist = JArray.Parse(CleanData(obj));
                        if (Inplist != null)
                        {
                            foreach (JObject u in Inplist)
                            {
                                string id = "";
                                string number = "";
                                string displayname = "";
                                string type = "";
                                string email = "";
                                string website = "";


                                foreach (JProperty p in u.Properties())
                                {
                                    if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                    if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                    if (p.Name.ToLower() == "displayname") { displayname = (string)p.Value; }
                                    if (p.Name.ToLower() == "type") { type = (string)p.Value; }
                                    if (p.Name.ToLower() == "email") { email = (string)p.Value; }
                                    if (p.Name.ToLower() == "website") { website = (string)p.Value; }

                                }

                                if (id != "")
                                {
                                    dynamic Unit = new ExpandoObject();
                                    Unit.Id = id;
                                    Unit.number = number;
                                    Unit.displayName = displayname;
                                    Unit.type = type;
                                    Unit.email = email;
                                    Unit.website = website;
                                    list.Add(Unit);
                                    if (website == Lbnr)
                                    {
                                        Userid = id;
                                    }
                                }

                            }
                        }
                    }
                }

                if (Userid != "")
                {
                    return Userid;
                }

                string currencyCode = "";
                string countryLetterCode = "";
                if (countryLetterCode == null || countryLetterCode == "" || countryLetterCode == " ")
                {
                    countryLetterCode = "DK";
                    currencyCode = "DKK";
                }

                if (CVR == null) { CVR = ""; }
                if (FirmName == null) { FirmName = ""; }
                if (FullName == null || FullName == "") { FullName = "?"; }
                if (Street == null) { Street = ""; }
                if (City == null) { City = ""; }
                if (Phone == null) { Phone = ""; }
                if (Email == null) { Email = ""; }
                if (Zip == null) { Zip = ""; }


                CVR = CVR.Replace("'", "''");
                FirmName = FirmName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                FullName = FullName.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                Street = Street.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                City = City.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                Phone = Phone.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                Zip = Zip.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                Email = Email.Replace("'", "''");
                Lbnr = Lbnr.Replace("'", "''");

                if (CVR != "" && CVR != " ") { CVR = CVR.Substring(0, CVR.Length > 20 ? 20 : CVR.Length); }
                if (FirmName != "" && FirmName != " ") { FirmName = FirmName.Substring(0, FirmName.Length > 100 ? 100 : FirmName.Length); }
                if (FullName != "" && FullName != " ") { FullName = FullName.Substring(0, FullName.Length > 100 ? 100 : FullName.Length); }
                if (Lbnr != "" && Lbnr != " ") { Lbnr = Lbnr.Substring(0, Lbnr.Length > 50 ? 50 : Lbnr.Length); } //name 2
                if (Street != "" && Street != " ") { Street = Street.Substring(0, Street.Length > 100 ? 100 : Street.Length); }
                if (City != "" && City != " ") { City = City.Substring(0, City.Length > 30 ? 30 : City.Length); }
                if (Phone != "" && Phone != " ") { Phone = Phone.Substring(0, Phone.Length > 30 ? 30 : Phone.Length); }
                if (Zip != "" && Zip != " ") { Zip = Zip.Substring(0, Zip.Length > 20 ? 20 : Zip.Length); }
                if (Email != "" && Email != " ") { Email = Email.Substring(0, Email.Length > 80 ? 80 : Email.Length); }

                try
                {

                    string LastModifiedDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    string url = ServerInstands + "companies(" + CompanyId + ")/" + entityName;

                    WebClient client = new WebClient();
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    client.Credentials = new System.Net.NetworkCredential(UserName, PassWord);

                    if (CVR != null && CVR != "" && CVR != " ")
                    {
                        var Company = new
                        {
                            type = "Company",
                            displayName = FullName + "(" + FirmName + ")",
                            phoneNumber = Phone,
                            email = Email,
                            website = Lbnr,
                            currencyCode = currencyCode,
                            taxRegistrationNumber = CVR,

                            address = new
                            {
                                street = Street,
                                city = City,
                                countryLetterCode = countryLetterCode,
                                postalCode = Zip
                            }
                        };

                        var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Company).Replace("\"", @""""));
                        var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
                        var items = jobj.Children().Cast<JProperty>().ToList();

                        foreach (JProperty p in items)
                        {
                            if (p.Name.ToLower() == "id") { Userid = (string)p.Value; }
                        }
                    }
                    else
                    {
                        var Person = new
                        {
                            type = "Person",
                            displayName = FullName,
                            phoneNumber = Phone,
                            email = Email,
                            website = Lbnr,
                            currencyCode = currencyCode,

                            address = new
                            {
                                street = Street,
                                city = City,
                                countryLetterCode = countryLetterCode,
                                postalCode = Zip
                            }
                        };

                        var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Person).Replace("\"", @""""));
                        var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
                        var items = jobj.Children().Cast<JProperty>().ToList();

                        foreach (JProperty p in items)
                        {
                            if (p.Name.ToLower() == "id") { Userid = (string)p.Value; }
                        }
                    }



                }
                catch (Exception ex)
                {
                    Userid = "-1";
                    //responseMessage = DataTransferObjects.ResponceHandler.Responce("Exception [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
                }

            }
            catch (Exception ex)
            {
                Userid = "-1";
                //responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace), 1, 2);
            }

            return Userid;
        }

        public string Navission_CreateProduct(string number, string displayname, string type, string UnitsOfMeasureCode, string priceincludestax, string unitprice, string unitcost, string taxgroupCode, string gtin)
        {
            string data = "";
            string productId = "";
            string entityName = "items";
            string lastmodifieddatetime = "";
            string symbol = "";
            string unitConversion = "";
            string blocked = "false";
            string UnitsOfMeasure = "";
            string baseUnitOfMeasureId = "";

            try
            {
               


                List<ExpandoObject> list = new List<ExpandoObject>();
                data = Navission_GetProduct(number);

                if (data.Contains("The remote server returned an error: (401) Unauthorized."))
                {
                    return productId = "-1";
                }

                if (data != null && data != "")
                {
                    JArray Inplist = JArray.Parse(data);
                    if (Inplist != null)
                    {
                        foreach (JObject u in Inplist)
                        {
                            string id = "";
                            number = "";
                            displayname = "";
                            type = "";
                            baseUnitOfMeasureId = "";
                            priceincludestax = "";
                            unitprice = "";
                            unitcost = "";
                            taxgroupCode = "";
                            lastmodifieddatetime = "";
                            gtin = "";

                            foreach (JProperty p in u.Properties())
                            {
                                if (p.Name.ToLower() == "id") { id = (string)p.Value; }
                                if (p.Name.ToLower() == "number") { number = (string)p.Value; }
                                if (p.Name.ToLower() == "displayname") { displayname = (string)p.Value; }
                                if (p.Name.ToLower() == "type") { type = (string)p.Value; }
                                if (p.Name.ToLower() == "baseunitofmeasureid") { baseUnitOfMeasureId = (string)p.Value; }
                                if (p.Name.ToLower() == "priceincludestax") { priceincludestax = (string)p.Value; }
                                if (p.Name.ToLower() == "unitprice") { unitprice = (string)p.Value; }
                                if (p.Name.ToLower() == "unitprice") { unitcost = (string)p.Value; }
                                if (p.Name.ToLower() == "taxgroupid") { taxgroupCode = (string)p.Value; }
                                if (p.Name.ToLower() == "lastmodifieddatetime") { lastmodifieddatetime = (string)p.Value; }
                                if (p.Name.ToLower() == "gtin") { gtin = (string)p.Value; }
                            }


                            productId = id;
                        }
                    }
                }

                if (productId != "")
                {
                    return productId;
                }

                string itemCategoryId = "00000000-0000-0000-0000-000000000000";
                string currencyCode = "";
                string countryLetterCode = "";
                if (countryLetterCode == null || countryLetterCode == "" || countryLetterCode == " ")
                {
                    countryLetterCode = "DK";
                    currencyCode = "DKK";
                }

                if (UnitsOfMeasureCode == "") { UnitsOfMeasureCode = "STK"; }
                if (UnitsOfMeasure == "") { UnitsOfMeasure = "Styk"; }
                 baseUnitOfMeasureId = Navission_GetUnitsOfMeasureId(UnitsOfMeasureCode);
                if (taxgroupCode == "") { taxgroupCode = "00000000-0000-0000-0000-000000000000"; } else { taxgroupCode = Navission_GetTaxGroupId(taxgroupCode); }
                //if (lastmodifieddatetime != "") { lastmodifieddatetime = DateTime.Now.ToString("MM/dd/yyyy H:mm"); }

                //HANDEL
                //CVR = CVR.Replace("'", "''");
                number = number.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                displayname = displayname.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
               // type = type.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
               // baseUnitOfMeasureId = baseUnitOfMeasureId;
                priceincludestax = priceincludestax.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                unitprice = unitprice.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "").Replace(@",", ".");
                unitcost = unitcost.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "").Replace(@",", ".");
               // taxgroupid = taxgroupid.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                //lastmodifieddatetime = lastmodifieddatetime.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");
                gtin = gtin.Replace("'", "''").Replace("-", "").Replace("/", "").Replace(")", "").Replace("(", "").Replace(@"\", "");

                priceincludestax = "True";
                if (priceincludestax.ToLower() == "true") { priceincludestax = "True"; } else { priceincludestax = "False"; }


                if (number != "" && number != " ") { number = number.Substring(0, number.Length > 20 ? 20 : number.Length); }
                if (displayname != "" && displayname != " ") { displayname = displayname.Substring(0, displayname.Length > 20 ? 20 : displayname.Length); }
                if (type != "" && type != " ") { type = type.Substring(0, type.Length > 20 ? 20 : type.Length); }

                if (gtin != "" && gtin != " ") { gtin = gtin.Substring(0, gtin.Length > 14 ? 14 : gtin.Length); }

                try { decimal d = Convert.ToDecimal(unitprice); } catch { return "-1"; }
                try { decimal d = Convert.ToDecimal(unitcost); } catch { return "-1"; }
                try { Guid d = Guid.Parse(baseUnitOfMeasureId); } catch { return "-1"; }



                try
                {

                    //string LastModifiedDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    string LastModifiedDateTime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ");

                    string url = ServerInstands + "companies(" + CompanyId + ")/" + entityName;

                    WebClient client = new WebClient();
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Encoding = Encoding.UTF8;
                    client.Credentials = new System.Net.NetworkCredential(UserName, PassWord);

                    string vat = "VAT Bus. Posting Group";
                    var Item = new
                    {
                        number = number,
                        displayName = displayname,
                        type = type,
                        itemCategoryId = itemCategoryId,
                        itemCategoryCode = "",
                        baseUnitOfMeasureId = baseUnitOfMeasureId,
                        blocked = Convert.ToBoolean(blocked),
                        priceIncludesTax = Convert.ToBoolean(priceincludestax),
                        unitPrice = Convert.ToDecimal(unitprice),
                        unitCost = Convert.ToDecimal(unitcost),
                        taxGroupId = taxgroupCode,
                        lastModifiedDateTime = LastModifiedDateTime,
                        gtin = gtin,
                        inventory = Convert.ToInt32("0"),

                         baseUnitOfMeasure = new
                         {
                             code = UnitsOfMeasureCode,
                             displayName = UnitsOfMeasure
                         }
                    };
                    //2019-05-22T19:37:14.193Z
                    string ee = JsonConvert.SerializeObject(Item).Replace("\"", @"""");
                    var dataString = client.UploadString(url, "POST", JsonConvert.SerializeObject(Item).Replace("\"", @""""));
                    var jobj = (JObject)JsonConvert.DeserializeObject(dataString.ToString());
                    var items = jobj.Children().Cast<JProperty>().ToList();

                    foreach (JProperty p in items)
                    {
                        if (p.Name.ToLower() == "id") { productId = (string)p.Value; }
                    }




                }
                catch (Exception ex)
                {
                    productId = "-1";
                    //responseMessage = DataTransferObjects.ResponceHandler.Responce("Exception [" + System.Reflection.MethodBase.GetCurrentMethod().Name + "]", "Message : " + ex.Message + Environment.NewLine + "StackTrace:" + ex.StackTrace, 1, 3);
                }

            }
            catch (Exception ex)
            {
                productId = "-1";
                //responseMessage = ResponceHandler.Responce("Error", DataFactory.GetInstans().METADATA_GET_ERROR(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace), 1, 2);
            }

            return productId;
        }


    }





    public enum ContainerType
    {
        Unknown = 0,
        Json = 1,
        XML = 2,
    }

    public enum ResponceType
    {
        Unknown = 0,
        Unauthorized = 1,
        Error = 2,
        InternalServerError = 3,
        ValidationFail = 4,
        Information = 5,
        Ok = 6,
        AlreadyExists = 7,
        NotFound = 8
    }

    public class Responce
    {
        public string Message { get; set; }

        public string Container { get; set; }
        public ContainerType ContainerType { get; set; }
        public ResponceType ResponceType { get; set; }

        public Responce(int Type = 1) //Defalt ContainerType is JSON = 1
        {
            Message = "";
            Container = "";
            ContainerType ct = (ContainerType)Enum.Parse(typeof(ContainerType), Type.ToString());
            ContainerType = ct;
            ResponceType = ResponceType.Unknown;
        }

        public Responce()
        {
            Message = "";
            Container = "";
            ContainerType ct = ContainerType.Json;
            ContainerType = ct;
            ResponceType = ResponceType.Unknown;
        }
    }


}